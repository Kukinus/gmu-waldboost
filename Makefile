# Projekt  Projekt pro predmet GMU: Akcelerace algoritmu WaldBoost pro detekci obliceju na GPU
# Autori   Daniel Sencuch, xsencu00
#          Dominik Zapletal, xzaple34
#          
# Datum    15. 12. 2016

#If your ICD loader and its header files (CL/cl.h) are in non-standard locations, you can say
#make OCL_INC=/somewhere/include OCL_LIBS=/somewhere/lib
OCL_INC=/usr/local/cuda-7.5/include
OCL_LIBS=/usr/local/cuda-7.5/lib64

OCL_INC_FLAGS=-I${OCL_INC}
OCL_LIB_FLAGS=-L ${OCL_LIBS} -lOpenCL

CXX=g++
CXXFLAGS=-std=c++11 -g `pkg-config --cflags opencv` ${OCL_INC_FLAGS}
LDLIBS=-lm -lboost_system -lboost_filesystem `pkg-config --libs opencv`



BIN_DIR=bin
SRC_DIR=src

TARGET1_NAME=face_det
TARGET1=${BIN_DIR}/${TARGET1_NAME}
MODNMS=nms
MODFD=face_detector
MODFDCPU=face_detector_cpu
MODOCL_UTIL=ocl_util
MODCLL=cll
MODFDGPU=face_detector_gpu
MODSW=stopwatch
MODTRACK=tracker
OBJ_FILES1=${TARGET1}.o ${BIN_DIR}/${MODNMS}.o ${BIN_DIR}/${MODFD}.o ${BIN_DIR}/${MODFDCPU}.o ${BIN_DIR}/${MODOCL_UTIL}.o ${BIN_DIR}/${MODCLL}.o ${BIN_DIR}/${MODFDGPU}.o ${BIN_DIR}/${MODSW}.o ${BIN_DIR}/${MODTRACK}.o
HEADER_FILES1=${SRC_DIR}/${MODNMS}.h ${SRC_DIR}/${MODFD}.h ${SRC_DIR}/${MODFDCPU}.h ${SRC_DIR}/${MODOCL_UTIL}.h ${SRC_DIR}/${MODCLL}.h ${SRC_DIR}/${MODFDGPU}.h ${SRC_DIR}/${MODSW}.h ${SRC_DIR}/${MODTRACK}.h

TARGET2_NAME=ocl_probe
TARGET2=${BIN_DIR}/${TARGET2_NAME}

TARGET3_NAME=face_det_nogpu
TARGET3=${BIN_DIR}/${TARGET3_NAME}
OBJ_FILES3=${TARGET3}.o ${BIN_DIR}/${MODNMS}.o ${BIN_DIR}/${MODFD}.o ${BIN_DIR}/${MODFDCPU}.o ${BIN_DIR}/${MODSW}.o ${BIN_DIR}/${MODTRACK}.o



# Automatic variables
# $@ - name of the TARGET1
# $< - name of the first prerequisite
# $^ - all prerequisites

all: ${TARGET1} ${TARGET2}

${BIN_DIR}/${MODNMS}.o: ${SRC_DIR}/${MODNMS}.cpp ${SRC_DIR}/${MODNMS}.h
	${CXX} ${CXXFLAGS} -c $< -o $@ ${LDLIBS}
   
${BIN_DIR}/${MODFD}.o: ${SRC_DIR}/${MODFD}.cpp ${SRC_DIR}/${MODFD}.h ${SRC_DIR}/${MODSW}.h
	${CXX} ${CXXFLAGS} -c $< -o $@ ${LDLIBS}

${BIN_DIR}/${MODFDCPU}.o: ${SRC_DIR}/${MODFDCPU}.cpp ${SRC_DIR}/${MODFDCPU}.h ${SRC_DIR}/${MODFD}.h ${SRC_DIR}/${MODSW}.h
	${CXX} ${CXXFLAGS} -c $< -o $@ ${LDLIBS}

${BIN_DIR}/${MODOCL_UTIL}.o: ${SRC_DIR}/${MODOCL_UTIL}.cpp ${SRC_DIR}/${MODOCL_UTIL}.h
	${CXX} ${CXXFLAGS} -c $< -o $@ ${OCL_LIB_FLAGS}

${BIN_DIR}/${MODCLL}.o: ${SRC_DIR}/${MODCLL}.cpp ${SRC_DIR}/${MODCLL}.h ${SRC_DIR}/${MODOCL_UTIL}.h
	${CXX} ${CXXFLAGS} -c $< -o $@ ${OCL_LIB_FLAGS}

${BIN_DIR}/${MODFDGPU}.o: ${SRC_DIR}/${MODFDGPU}.cpp ${SRC_DIR}/${MODFDGPU}.h ${SRC_DIR}/${MODFD}.h ${SRC_DIR}/${MODCLL}.h ${SRC_DIR}/${MODSW}.h
	${CXX} ${CXXFLAGS} -c $< -o $@ ${LDLIBS} ${OCL_LIB_FLAGS}

${BIN_DIR}/${MODSW}.o: ${SRC_DIR}/${MODSW}.cpp ${SRC_DIR}/${MODSW}.h
	${CXX} ${CXXFLAGS} -c $< -o $@ 

${BIN_DIR}/${MODTRACK}.o: ${SRC_DIR}/${MODTRACK}.cpp ${SRC_DIR}/${MODTRACK}.h
	${CXX} ${CXXFLAGS} -c $< -o $@ 

${TARGET1}.o: ${SRC_DIR}/main.cpp ${SRC_DIR}/${MODFD}.h ${SRC_DIR}/${MODFDCPU}.h ${SRC_DIR}/${MODFDGPU}.h ${SRC_DIR}/${MODTRACK}.h
	${CXX} ${CXXFLAGS} -c $< -o $@ ${LDLIBS}

${TARGET1}: ${OBJ_FILES1}
	${CXX} ${CXXFLAGS} $^ -o $@ ${LDLIBS} ${OCL_LIB_FLAGS}



${TARGET2}: ${SRC_DIR}/${TARGET2_NAME}.cpp
	$(CXX) ${CXXFLAGS} $< -o $@ $(OCL_LIB_FLAGS)



${TARGET3}.o: ${SRC_DIR}/main.cpp ${SRC_DIR}/${MODFD}.h ${SRC_DIR}/${MODFDCPU}.h ${SRC_DIR}/${MODTRACK}.h
	${CXX} ${CXXFLAGS} -DNO_GPU -c $< -o $@ ${LDLIBS}

${TARGET3}: ${OBJ_FILES3}
	${CXX} ${CXXFLAGS} -DNO_GPU $^ -o $@ ${LDLIBS}
    
# bin/optflowFarneback: ${SRC_DIR}/optflowFarneback.cpp
# 	$(CXX) ${CXXFLAGS} $< -o $@ ${LDLIBS} $(OCL_LIB_FLAGS)

# bin/optflowLK2: ${SRC_DIR}/optflowLK2.cpp
# 	$(CXX) ${CXXFLAGS} $< -o $@ ${LDLIBS}

run:
	./bin/face_det ./data/foto3.jpg ./src/
   
pack:
	zip -r xsencu00-xzaple34.zip ./${SRC_DIR}
   
clean:
	rm -f ${BIN_DIR}/*.o ${TARGET1} ${TARGET2} ${TARGET3}
