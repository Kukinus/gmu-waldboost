Dobrý den,

zadání jsem změnil. Přidání člena 3 člena týmu je možné. Samozřejmě mějte na paměti, že čím více členů tím lepší výsledek očekáváme. Teď k těm informacím. Co se týká samotných boostovacích algoritmů s Random forest příznaky je popsán zde v sekci Cascaded Random Forest for Fast Object Detection:

http://link.springer.com/chapter/10.1007/978-3-642-38886-6_13

Rozdíl v klasifikátoru, který jsem Vám přiložil spočívá v tom, že budete mít jen 1 strom/stage.

Jsou zde odkazy i na další relevantní literaturu. Základ vychází v Viola and Jonese:

http://ieeexplore.ieee.org/document/990517/

a vylepšení prostřednicvím WaldBoostu:

http://ieeexplore.ieee.org/document/1467435/

Gpu implementace WaldBoostu s LRD příznaky:

http://link.springer.com/article/10.1007/s11554-010-0179-0

Co se týká natrénovaného klasifikátoru mám tu interní CPU implementaci s natrénovaným klasifikátorem. Klasifikátor se skádá ze 4 souborů:
fids - pozice v rámci detekčního okna - 3 čísla na řádku / strom -> hloubka 3 včetně listů
thrs - jsou prahy pro jednotlivé pozice - 3 čísla na řádku / strom -> hloubka 3 včetně listů
qhs - listový likelihood, který se má přičíst do průběžné sumy
shrink - jedno číslo - kolikrát se má zmenšovat

Prahování probíhá od pomocí lineární křivky kq + q kde k=0.5 a q=-1024. S Vaším klasifikátorem o 2048 stage to znamená, že začíná na -1024 a končí na 1024. Na detaily implementace se můžete podívat v přiloženém zdrojovém kódu. Scalů je v implementaci jen 8 na pevno. V reálu by bylo dobré dělat scaly do té doby dokud je v obraze alespoň 1 detekční okno. Doporučuji nastudovat ozkoušet a zastavit se na konzultaci s dotazy a případným návrhem implementace.

S pozdravem
Michal Kula 








Další zdroj:
https://en.wikipedia.org/wiki/Prefix_sum