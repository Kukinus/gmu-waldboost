#include <stdint.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>


using namespace std; 
using namespace cv;

template <typename T> 
class MImage{

public:	
	int width;
	int height;	
	T *im;
	
	MImage(int w,int h){ 
		width = w; 
		height = h; 
		im = new T[width*height];
	}

	MImage(Mat m){
		height = m.rows;
		width = m.cols;		
		im = new T[width*height];
		
		for(int y = 0;y < height;y++){
			for(int x = 0;x < width;x++){	
				at(y,x) = m.ptr<short>(y)[x];
			}
		}
	}
	//*************************************************************************

	T operator () (int y,int x)const{ //GET value
		if(y<0) y=0;
		if(y>height-1)y=height-1;
		if(x<0) x=0;
		if(x>width-1) x=width-1;
		return im[(y*width)+x];}


	T& operator () (int y,int x){  //SET value
		if(y<0) y=0;
		if(y>height-1)y=height-1;
		if(x<0) x=0;
		if(x>width-1) x=width-1;
		return im[(y*width)+x];}

	 
	T at(int y,int x)const{ //GET value
		if(y<0) y=0;
		if(y>height-1)y=height-1;
		if(x<0) x=0;
		if(x>width-1) x=width-1;
		return im[(y*width)+x];}
	

	T& at(int y,int x){  //SET value
		if(y<0) y=0;
		if(y>height-1)y=height-1;
		if(x<0) x=0;
		if(x>width-1) x=width-1;
		return im[(y*width)+x];}
	//***********************************************************************

	MImage<T> shrink(int shrink=2){
		MImage out(width/shrink,height/shrink);

		for(int y = 0;y < height/shrink;y++)
			for(int x = 0;x < width/shrink;x++){		
				out(y,x) = 0.0;			
				for(int yy = 0;yy < shrink; yy++)
					for(int xx = 0;xx < shrink; xx++){
						out(y,x)  += at((y*shrink+yy),x*shrink+xx);
					}
				out(y,x) /= (shrink*shrink);
			}

		return out;
	}
	//***********************************************************************

	MImage<T> triangle(){

		MImage out(width,height);
		T filter[3][3] = {{ 1,2,1},{2,4,2},{1,2,1}};

		for(int y = 0;y < height;y++)
			for(int x = 0;x < width;x++){		
				out(y,x) = 0;
				T dif = 16;			
				for(int yy = -1;yy <= 1; yy++){
					for(int xx = -1;xx <= 1; xx++){
						out(y,x) += at((y+yy),x+xx)*filter[yy+1][xx+1];
										
					}
				}
				out(y,x) /= dif;
			}
		return out;
	}
	//***********************************************************************

	MImage<T> diffx() {
		MImage out(width,height);
		for(int y = 0;y < height;y++){
			for(int x = 0;x < width;x++){	
				if(x==0){
					out(y,x) = int(at(y,x+1)-at(y,x));
				}
				else if(x == width-1){
					out(y,x) = int(at(y,x)-at(y,x-1));
				}
				else{
					out(y,x) = int((at(y,x+1)-at(y,x-1))/ 2);
				}
			}
		}		
		return out;
	}


	MImage<T> diffy() {
		MImage out(width,height);	
		for(int y = 0;y < height;y++){
			for(int x = 0;x < width;x++){	
				if(y==0){
					out(y,x) = int(at(y+1,x)-at(y,x));
				}
				else if(y == height-1){
					out(y,x) = int(at(y,x)-at(y-1,x));
				}
				else{
					out(y,x) = int((at(y+1,x)-at(y-1,x))/2);
				}
			}
		}		
		return out;
	}
	//*******************************************************************************

	void printF(){
		for(int y = 0;y < height;y++){
			for(int x = 0;x < width;x++){
				printf("%4.4f ",im[y*width + x]);
			}
			cout << endl;	
		}	
	}


	void printI(){
		for(int y = 0;y < height;y++){
			for(int x = 0;x < width;x++){
				printf("%4d ",(int)round(im[y*width + x]));
			}
			cout << endl;	
		}	
	}
};

