#ifndef _NMS_H_
#define _NMS_H_

#include <vector>
#include <opencv2/opencv.hpp>

void nmsMaxLinearImproved(std::vector<cv::Rect> & rects, std::vector<float> & hs, float ovr = 0.5, int minGroup = 1);

#endif
