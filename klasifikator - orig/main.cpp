/* Tento zdrojový kód a přilohy jsou předmětem autorských práv FIT VUT v Brně. Pro případné komerční využití a šíření je vyžadován písemný souhlas CEMC. Bez vědomí této organizace je zakázáno aplikaci kopírovat a dále distribuovat.
/* Tyto zdrojové kody a přílohy jsou poskytnuty pro spolupráci firmě Camea spol s r.o. a Eyedea Recognition s r.o. */

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <math.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>

#include "image.hpp"
#include "nms.h"

using namespace std; 
using namespace cv;


//************************************************************************
#define DEFAULT_PATH	""
#define DEFAULT_IMAGE	"foto3.jpg"


#define FEATURE_COUNT	2048
#define FEATURE_LIMIT	1500
#define WINDOW_WIDTH	64
#define WINDOW_HEIGTH	64
#define TREE_DEPTH		2
#define FIDS_LIMIT		3
#define QHS_LIMIT		4

#define Q		-1024
#define K		0.5

#define SCALE_INITIAL	(1.5)
#define SCALE_RATIO		(7.0/8.0)
#define SCALE_RATIO_INV		(1.0/SCALE_RATIO)


//****************************************************************
void loadData( const char* name, int *data);
void getCoords(int pos,int shrink, int &chan, int &x, int &y);
//****************************************************************

int main( int argc, char* argv[]){

	String foto;
	String path;
	if(argc>=2)
		foto = argv[1];	
	else
		foto = DEFAULT_IMAGE;
	if(argc>=3)
		path = argv[2]+String("/");
	else
		path = DEFAULT_PATH;	

	Mat image = imread(foto,CV_LOAD_IMAGE_GRAYSCALE);
	Mat draw_image = imread(foto);
	Mat gray;	
	image.convertTo(gray,CV_16S);
		
	Mat out;
	std::vector<Rect> rects;
	std::vector<float> out_hs;
  std::vector<Rect> rects_temp;
  std::vector<float> out_hs_temp;
	
	resize(gray, gray, Size(), SCALE_INITIAL, SCALE_INITIAL, INTER_LINEAR);	
	resize(draw_image, draw_image, Size(), SCALE_INITIAL, SCALE_INITIAL, INTER_LINEAR);	
	
	
	cout<<"Resolution: "<<gray.rows<<" x "<<gray.cols <<" dims "<< gray.dims<<endl;
	
	int *fids = new int[FEATURE_COUNT*FIDS_LIMIT];
	int *thrs = new int[FEATURE_COUNT*FIDS_LIMIT];
	int *qhs = new int[FEATURE_COUNT*QHS_LIMIT];
	int shrink; 
	
	
	
	loadData((path+String("fids")).c_str(),fids);
	loadData((path+String("thrs")).c_str(),thrs);
	loadData((path+String("qhs")).c_str(),qhs);
	loadData((path+String("shrink")).c_str(),&shrink);
	printf("%d\n",shrink);
	int stat_pos = 0;
	int stat_feature = 0;	
	int stat_detect = 0;		
	
	int max_suma = 0;
		
	for(int scale = 0; scale < 8; scale++){		
		MImage<float> img(gray);
		MImage<float> chan[] = {img.triangle().shrink(shrink), img.triangle().diffx().shrink(shrink), img.triangle().diffy().shrink(shrink)};
		
		for(int y = 0; y < chan[0].height - WINDOW_HEIGTH/shrink;y+=2){
			for(int x = 0; x < chan[0].width - WINDOW_WIDTH/shrink;x+=2){	
				stat_pos++;
				int suma = 0;
				for(int i = 0; i < FEATURE_LIMIT; i++){
					int node = 0;
					stat_feature++;
					for(int l = 0; l < TREE_DEPTH; l++){
						int pos = fids[i*FIDS_LIMIT+ node];
						int th = thrs[i*FIDS_LIMIT+ node];
						int chan_c, x_c, y_c;
						getCoords(pos,shrink,chan_c,x_c,y_c);
						float val = chan[chan_c](y+y_c,x+x_c);					
					
						if(val < th){
							node = node*2+1;
						}
						else{
							node = node*2+2; 
						}
					}
				
					int qhs_i = i*QHS_LIMIT+ (node-3);
					int hs = qhs[qhs_i];
					suma+= hs;
				
					if(suma < (K*i+Q))
						break;
					if(i == FEATURE_LIMIT-1){					
						cout<<"y: "<<y<<" x: "<<x<<" s: "<<scale<<" suma: "<<suma<<endl;
						int s = scale;
						Rect pt;
						pt.x = shrink*x;
						pt.y = shrink*y;
						pt.width = WINDOW_WIDTH;
						pt.height = WINDOW_HEIGTH;
						while(s > 0){
							pt.x= pt.x*SCALE_RATIO_INV;
							pt.y= pt.y*SCALE_RATIO_INV;
							pt.width = pt.width *SCALE_RATIO_INV;
							pt.height = pt.height *SCALE_RATIO_INV;
							s--;
						}
						
						rects.push_back(pt);
						out_hs.push_back(suma);
						stat_detect++;
            if(suma > max_suma) max_suma = suma;
					}
				
				}
			}
		}
		resize(gray, gray, Size(), SCALE_RATIO, SCALE_RATIO, INTER_LINEAR);	
	}
  // draw all detection
  //for(int i = 0; i < rects.size(); i++) rectangle(draw_image, rects[i], Scalar(255.0f * out_hs[i] / max_suma, 255.0f * out_hs[i] / max_suma, 255.0f * out_hs[i] / max_suma));
  rects_temp = rects;
  out_hs_temp = out_hs;
  nmsMaxLinearImproved(rects_temp, out_hs_temp);
  for(int i = 0; i < rects_temp.size(); i++){
    rectangle(draw_image, rects_temp[i], Scalar(0, 255, 0));
  }

	printf("%d  %d  %f  %d\n",stat_pos,stat_feature, stat_feature/(stat_pos*1.0), stat_detect);	
	imshow( "CAMERA", draw_image);
	waitKey();
	return 0;
}


void getCoords(int pos, int shrink, int &chan, int &x, int &y){

	chan = pos /((WINDOW_WIDTH/shrink)*(WINDOW_HEIGTH/shrink));
	x = (pos / (WINDOW_WIDTH/shrink)) % (WINDOW_HEIGTH/shrink);
	y = pos % (WINDOW_WIDTH/shrink);
}


void loadData( const char* name, int *data){

	int position = 0;	
	int number;		
	fstream fp;
	
	fp.open(name,fstream::in);
	if(fp.is_open()){
  		while (fp >> number)
			  {
				data[position++] = number;
			  }
	}
	else{
		printf("Cannot load file: %s\n",name);		
	}
	fp.close();
}
