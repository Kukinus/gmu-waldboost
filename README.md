Projekt do předmětu GMU (Grafické a multimediální procesory) a POV (Počítačové vidění) na FIT VUT. Jedná se o Akceleraci detekce objektů pomocí algoritmu WaldBoost na GPU (pro GMU) a trackování těchto detekcí (pro POV).

Autoři: Dominik Zapletal, xzaple34			- akcelerace
		Daniel Senčuch, xsencu00			- akcelerace, tracking


*******************************
*         Dokumentace		  *
*******************************

Všechny uvedené příkazy pro CLI předpokládají aktuální pracovní složku v kořenové složce projektu (obsahuje složky bin, src, data atd.), pokud nebude uvedeno jinak.

Implementace detekce na GPU je stále otevřená (deadline je až 8. 1.); tato dokumentace o ní nebude pojednávat. Je ale pořád velmi naivní, neoptimalizovaná a je v ní nepořádek. Přesto stíhá zpracovávat FullHD snímky za 60 ms (Nvidia GeForce 660 Ti).

Překlad a spuštění
==================

Pro překlad je téměř nutné mít správně nainstalované knihovny OpenCV 3 a OpenCL a mít v počítači hardware, který OpenCL podporuje. Program jde přeložit i bez OpenCL příkazem

make bin/face_det_nogpu

ale CPU implementace detektoru, ke které se pak program musí uchýlit, je extrémně pomalá.
Jinak lze program přeložit klasicky příkazem make. Spustitelné soubory najdete ve složce bin.


Synopse programu je následující:

face_det [--CPU] input_file classifier_data_folder";
	--CPU 					use CPU implementation of the classifier
	input_file				path to an image file or video
	classifier_data_folder	path to folder with tree data files
							(fids, qhs, thrs)

classifier_data_folder by měla být nastavena na ./src/

Nejlepší způsob spuštění detektoru a předvedení trackeru (funguje jen u video souborů) je příkazem

./bin/face_det ./data/drunken_master.mp4 ./src/

Tracker by měl otevřít dvě okna: V prvním ("Face detector") zobrazí snímky videa s obdélníky vrácenými detektorem obličejů, ve druhém ("Tracker") zobrazí body a obdélníky sledované Trackerem na základě obdélníků z detektoru.


Tracker
=======

Třída Tracker je implementována v souborech tracker.cpp a tracker.h. Tato třída je instanciována a používána pouze v souboru main.cpp, samozřejmě pouze pokud se aplikaci předala cesta k videu, ne obrázku. 

Jako vstup předpokládá Tracker snímek videa konvertovaný do HSV barevného prostoru, černobílý snímek a vektor obdélníků, které vytvořil detektor obličejů a které mají vyznačovat oblast výskytu obličejů ve snímku.

Tracker vytvoří trackovatelné (klíčové) body pomocí funkce OpenCV goodFeaturesToTrack(). Body jsou vytvořeny jen v oblastech vyznačených předanými obdélníky a začne jejich sledování. V dalším příchozím snímku je vypočítána nová poloha těchto bodů pomocí optického toku - funkce OpenCV calcOpticalFlowPyrLK()  (pyramidová metoda Lucas-Kanade).

Struktura Track:
Reprezentuje jeden sledovaný obličej. Jedna tato struktura obsahuje:
- identifikátor
- počáteční a koncový index v poli všech sledovaných bodů, které danému Tracku náleží
- obdélník, který má vyznačit polohu sledovaného obličeje
- Poloha obdélníku je ve skutečnosti definována odchylkou od průměrné polohy všech příslušných sledovaných bodů. Tato odchylka je součástí struktury.
- "Time to live" - počet následujících snímků, ve kterých může Track ještě existovat.

Tracker tedy má za úkol udržovat správnou strukturu bodů a k nim příslušejících Tracků, a to je předmětem většiny jeho kódu. Stará se o body, které byly při trackování ztraceny, ničení tracků, jejichž doba života uplynula atd. Bohužel je v ve správě Tracků bug a občas v nich nekonzistence vznikají. Z tohoto důvodu je občas nutné veškeré Tracky smazat a začít Trackovat znova (od aktuálního snímku).

Kromě toho je třeba také detekovat kompletní změnu scény (např. střih ve filmu) a případně smazat všechny aktuální Tracky. Detekce změny scény probíhá porovnáním histogramů HSV obrazu předchozího a aktuálního snímku. Rozdíl se počítá pomocí "Chi-square" vzdálenosti. Ten se pak porovná s prahem a pokud je větší, je mezi snímky tak razantní rozdíl, že pravděpodobně došlo ke změně scény.


Závěr
=====

S procesorem Intel Core i5 a grafickou kartou nVidia GeForce 660 Ti a snímkem o rozlišení 1280x720 px trvá detekce obličejů zhruba 60 ms, tracking 25-60 ms.

Při hodnocení efektivity Trackeru je třeba vzít v úvahu i nedokonalost detektoru obličejů, který Trackeru obdélníky posílá. CPU implementace detektoru nám byla předána a při akceleraci na GPU jsme ji pouze napodobovali. Tato metoda detekce je z principu poměrně nespolehlivá.

Při implementaci trackeru i samotné akcelerace jsem narazil na několik těžkostí, které vedly ke značnému zdržení postupu a výslednému nedodržení termínu odevzdání do předmětu POV a nepořádku ve výsledném kódu. Za veškeré komplikace se nesmírně omlouvám a děkuji, pokud i přesto budete ochotný se mým projektem zabývat. Doufám, že se mi povede tyto komplikace vyjasnit na obhajobě projektu.







