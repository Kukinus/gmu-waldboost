/**
 * Requires the OpenCL SDK and C++ bindings.
 */

#include <string>
#include <vector>
#include <iostream>
using namespace std;

#if defined(__APPLE__) || defined(__MACOSX)
#include <OpenCL/cl.hpp>
#else
#include <CL/cl.hpp>
#endif

void dumpFPConfig(cl_device_fp_config bitfield) {
  if (bitfield & CL_FP_DENORM) {
    cout << "   Denorms are supported." << endl;
  }
  if (bitfield & CL_FP_INF_NAN) {
    cout << "   INF and NaNs are supported." << endl;
  }
  if (bitfield & CL_FP_ROUND_TO_NEAREST) {
    cout << "   Round to nearest even rounding mode supported." << endl;
  }
  if (bitfield & CL_FP_ROUND_TO_ZERO) {
    cout << "   Round to zero mode supported." << endl;
  }
  if (bitfield & CL_FP_ROUND_TO_INF) {
    cout << "   Round to +ve and -ve infinity rounding mode supported." << endl;
  }
  if (bitfield & CL_FP_FMA) {
    cout << "   IEEE754-2008 fused multiply-add is supported." << endl;
  }          
}

void dumpDevice(cl::Device device, bool brief = false) {
  std::string name, vendor, profile, version, extensions;
  device.getInfo<string>(CL_DEVICE_NAME, &name);
  device.getInfo<string>(CL_DEVICE_VENDOR, &vendor);  
  device.getInfo<string>(CL_DEVICE_PROFILE, &profile);
  device.getInfo<string>(CL_DEVICE_VERSION, &version);
  device.getInfo<string>(CL_DEVICE_EXTENSIONS, &extensions);

  cl_bool available, compAvailable, littleEndian, errCorrect, imgSupport;
  device.getInfo<cl_bool>(CL_DEVICE_AVAILABLE, &available);
  device.getInfo<cl_bool>(CL_DEVICE_COMPILER_AVAILABLE, &compAvailable);
  device.getInfo<cl_bool>(CL_DEVICE_ENDIAN_LITTLE, &littleEndian);
  device.getInfo<cl_bool>(CL_DEVICE_ERROR_CORRECTION_SUPPORT, &errCorrect);
  device.getInfo<cl_bool>(CL_DEVICE_IMAGE_SUPPORT, &imgSupport);

  cl_uint addrBits, maxClock, maxCompUnits, maxConstArgs, maxParmSize,
    maxSamplers, maxWorkGrpSize, maxWorkItmDim, globalMemCacheline;
  device.getInfo<cl_uint>(CL_DEVICE_ADDRESS_BITS, &addrBits);
  device.getInfo<cl_uint>(CL_DEVICE_MAX_CLOCK_FREQUENCY, &maxClock);
  device.getInfo<cl_uint>(CL_DEVICE_MAX_COMPUTE_UNITS, &maxCompUnits);
  device.getInfo<cl_uint>(CL_DEVICE_MAX_CONSTANT_ARGS, &maxConstArgs); 
  device.getInfo<cl_uint>(CL_DEVICE_MAX_PARAMETER_SIZE, &maxParmSize);
  device.getInfo<cl_uint>(CL_DEVICE_MAX_SAMPLERS, &maxSamplers);
  device.getInfo<cl_uint>(CL_DEVICE_MAX_WORK_GROUP_SIZE, &maxWorkGrpSize); 
  device.getInfo<cl_uint>(CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, &maxWorkItmDim);
  device.getInfo<cl_uint>(CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE, &globalMemCacheline);   

  cl_ulong maxConstBufSize, maxMemAllocSize, globalMemCacheSize, globalMemSize,
    localMemSize;
  device.getInfo<cl_ulong>(CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, &maxConstBufSize);
  device.getInfo<cl_ulong>(CL_DEVICE_MAX_MEM_ALLOC_SIZE, &maxMemAllocSize);
  device.getInfo<cl_ulong>(CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, &globalMemCacheSize);
  device.getInfo<cl_ulong>(CL_DEVICE_GLOBAL_MEM_SIZE, &globalMemSize);
  device.getInfo<cl_ulong>(CL_DEVICE_LOCAL_MEM_SIZE, &localMemSize);    

  vector<size_t> workItemSizes;
  device.getInfo<vector<size_t> >(CL_DEVICE_MAX_WORK_ITEM_SIZES, &workItemSizes);

  cl_device_fp_config dfpConfig, sfpConfig;
  device.getInfo<cl_device_fp_config>(CL_DEVICE_DOUBLE_FP_CONFIG, &dfpConfig);
  device.getInfo<cl_device_fp_config>(CL_DEVICE_SINGLE_FP_CONFIG, &sfpConfig);

  cl_device_exec_capabilities execCap;
  device.getInfo<cl_device_exec_capabilities>(CL_DEVICE_EXECUTION_CAPABILITIES,
                                              &execCap);

  cl_device_mem_cache_type memCacheType;
  device.getInfo<cl_device_mem_cache_type>(CL_DEVICE_GLOBAL_MEM_CACHE_TYPE,
                                           &memCacheType);

  cl_device_local_mem_type localMemType;
  device.getInfo<cl_device_local_mem_type>(CL_DEVICE_LOCAL_MEM_TYPE, &localMemType);

  cl_command_queue_properties cmdQueueProps;
  device.getInfo<cl_command_queue_properties>(CL_DEVICE_QUEUE_PROPERTIES,
                                              &cmdQueueProps);

  cout << "Name: " << name << endl
       << "Vendor: " << vendor << endl;

  if (brief) return;
  
  cout << "Profile: " << profile << endl
       << "Version: " << version << endl
       << "Extensions: " << extensions << endl
       << "Available: " << (available ? "YES" : "NO") << endl    
       << "Compiler available: " << (compAvailable ? "YES" : "NO") << endl
       << "Endianess: " << (littleEndian ? "Little" : "Big") << endl
       << "Error correction support: " << (errCorrect ? "YES" : "NO") << endl
       << "Image support: " << (imgSupport ? "YES" : "NO") << endl
       << "Address space: " << addrBits << " bits" << endl
       << "Global memory size: " << globalMemSize << " bytes" << endl
       << "Global memory cache: ";

  if (memCacheType == CL_NONE) {
    cout << "none" << endl;
  }
  else if (memCacheType == CL_READ_ONLY_CACHE) {
    cout << "read-only" << endl;
  }
  else if (memCacheType == CL_READ_WRITE_CACHE) {
    cout << "read-write" << endl;
  }

  if (memCacheType != CL_NONE) {
    cout << "   Size: " << globalMemCacheSize << " bytes" << endl
         << "   Cache line: " << globalMemCacheline << " bytes" << endl;
  }
  
  cout << "Local memory: " << endl
       << "   Type: " << (localMemType == CL_LOCAL ? "local" : "global") << endl
       << "   Size: " << localMemSize << " bytes" << endl
       << "Max clock frequency: " << maxClock << " MHz" << endl
       << "Max compute units: " << maxCompUnits << endl
       << "Max __constant arguments: " << maxConstArgs << endl
       << "Max buffer size: " << maxConstBufSize << " KB" << endl
       << "Max memory allocation: " << maxMemAllocSize << " bytes" << endl
       << "Max kernel arguments: " << maxParmSize << endl
       << "Max samplers pr. kernel: " << maxSamplers << endl
       << "Max number of work-items in a work-group: " << maxWorkGrpSize << endl
       << "Max dimensions for global and local work-items: " << maxWorkItmDim << endl
       << "Max work-items pr. dimension: (";

  size_t tot = workItemSizes.size();
  for (size_t i = 0; i < tot; i++) {
    cout << workItemSizes[i];
    if (i < tot - 1) cout << ", ";
  }
  
  cout << ")" << endl
       << "Double precision floating-point capabilities:" << endl;
  dumpFPConfig(dfpConfig);
  cout << "Single precision floating-point capabilities:" << endl;
  dumpFPConfig(sfpConfig);

  cout << "Execution capabilities:" << endl;
  if (execCap & CL_EXEC_KERNEL) {
    cout << "   Can execute OpenCL kernels." << endl;
  }
  if (execCap & CL_EXEC_NATIVE_KERNEL) {
    cout << "   Can execute native kernels." << endl;
  }

  cout << "Command queue properties:" << endl;
  if (cmdQueueProps & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE) {
    cout << "   Out-of-order execution enabled." << endl;
  }
  if (cmdQueueProps & CL_QUEUE_PROFILING_ENABLE) {
    cout << "   Profiling enabled." << endl;
  }    
}

void dumpPlatform(int type, cl::Platform platform, bool brief = false) {
  cl_context_properties properties[] = 
    { CL_CONTEXT_PLATFORM, (cl_context_properties) (platform)(), 0};

  cl::Context context = cl::Context(type, properties);
  vector<cl::Device> devices = context.getInfo<CL_CONTEXT_DEVICES>();
  if (devices.size() == 0) {
    cerr << "Could not find any devices of this type." << endl << endl;
    return;
  }

  for (int i = 0; i < devices.size(); i++) {
    if (!brief) {
      cout << "Device " << i << ":" << endl;
    }
    
    dumpDevice(devices[i], brief);
    cout << endl;
  }
}

int main(int argc, char **argv) {
  vector<cl::Platform> platforms;
  int err = cl::Platform::get(&platforms);
  if (err != CL_SUCCESS || platforms.size() == 0) {
    cerr << "Could not get available platforms." << endl;
    return -1;
  }

  for(size_t i = 0; i < platforms.size(); i++) {
    cl::Platform platform = platforms[i];
    std::string name, vendor, profile, version;
    platform.getInfo(CL_PLATFORM_NAME, &name);
    platform.getInfo(CL_PLATFORM_VENDOR, &vendor);
    platform.getInfo(CL_PLATFORM_PROFILE, &profile);
    platform.getInfo(CL_PLATFORM_VERSION, &version);    
    cout << "Platform " << name << " (" << i << ") vendor = " << vendor
         << ", profile = " << profile << ", version = " << version
         << endl << endl;

    cout << "=== GPUs ===" << endl;
    dumpPlatform(CL_DEVICE_TYPE_GPU, platform);

    cout << "=== CPUs ===" << endl;
    dumpPlatform(CL_DEVICE_TYPE_CPU, platform);

    cout << "=== Accelerators ===" << endl;
    dumpPlatform(CL_DEVICE_TYPE_ACCELERATOR, platform);

    cout << "=== Default device ===" << endl;
    dumpPlatform(CL_DEVICE_TYPE_DEFAULT, platform, true);    
  }

  return 0;
}
