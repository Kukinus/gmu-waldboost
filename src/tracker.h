#ifndef _TRACKER_H_
#define _TRACKER_H_


#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/videoio/videoio.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <iostream>
#include <ctype.h>
#include <list>

using namespace cv;
using namespace std;

struct Track {
	const size_t id;
	cv::Rect rect;
	Point2f offset_from_avg;
	size_t original_point_cnt;
	size_t start;
	size_t end;
	char time_to_live;
	//std::vector<int> point_indices;
};


class Tracker {
public:

	Tracker();
	~Tracker();

	void nextFrame( const Mat &frame_gray, const Mat &frame_hsv );

	bool includeRects(const vector<Rect> &rects);

	void visualizeTracks( Mat &img );



private:

	// Constants for the goodFeaturesToTrack() function
	static const int MAX_FEATURE_COUNT;
	static const float FEATURE_QUALITY;
	static const int MIN_FEATURE_DISTANCE;
	static const int BLOCK_SIZE;
	static const bool USE_HARRIS;
	static const float K;
	static const cv::Size _subPixWinSize;

	// Constants for the calcOpticalFlowPyrLK() function
	static const int MAX_PYR_LEVEL;			// Cannot init static const objects 
	static const cv::TermCriteria TERM_CRIT; 	// in class declaration. They need to be 
	static const cv::Size _winSize;			 	// initialized in the .cpp file.
	static const float MIN_EIG_THRESH;

	static const char STARTING_TIME_TO_LIVE;

	static const bool _DEBUG;

	Mat _prevGray;
	Mat _prevHSV;
	MatND _prevHSVHistogram;
	std::vector< std::vector<cv::Point2f> > _points;

	size_t _nextID;
	std::list<struct Track> _tracks;

	bool _nightMode;



	size_t getNewID();
	bool getOverlap( const Rect &r, std::vector< std::list<struct Track>::iterator > &track_it, size_t &retaken_id);

	void processPoints(std::vector<cv::Point2f> &points, std::vector<uchar> &keepThisPoint);

	bool sceneChanged( const Mat &img_hsv );

	void deleteTrack( std::list<struct Track>::iterator &track_it );
	void clearTracks();
	bool checkTrackConsistency();
	void printTracks();
	void printTrack(const struct Track &t);

};


#endif // _TRACKER_H_