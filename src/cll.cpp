#include "cll.h"

void CL::checkError()
{
    if(_err != CL_SUCCESS) {
        throw std::exception();
    }
}


/********************* v Constructor & Destructor v ************************/

CL::CL(const std::string &kernel_path, const int window_width, const int window_height, const int feature_limit, const int fids_limit, const int tree_depth, const int qhs_limit, const float k, const int q, const int shrink) :
    _uint8_format(CL_INTENSITY, CL_UNSIGNED_INT8),
    _sint16_format(CL_INTENSITY, CL_SIGNED_INT16),
    _float_format(CL_R, CL_FLOAT),
    _platform_used(0),
    _device_used(0),
    _kernel_source_path(kernel_path),
    _verbose(true),
    _image_loaded(false),
    _shrink(1),
    _cl_images_scaled(2)
    {
    if(_kernel_source_path[_kernel_source_path.length() - 1] != '/') _kernel_source_path.append("/");
    _kernel_source_path += std::string("kernel.cl");

    if(_verbose) printf("Initialize OpenCL object and context\n");
    //setup devices and context
    
    //oclErrorString is defined in ocl_util.cpp and comes from the NVIDIA SDK
    ///if(_verbose) printf("oclGetPlatformID: %s\n", oclErrorString(_err));
    _err = cl::Platform::get(&_platforms);
    if(_verbose) printf("cl::Platform::get(): %s\n", oclErrorString(_err));
    if(_verbose) printf("number of platforms: %lu\n", _platforms.size());
    if (_err != CL_SUCCESS || _platforms.size() == 0) {
        std::cerr << "Could not get available platforms." << std::endl;
    }

    //create the context
    cl_context_properties properties[] =
    { CL_CONTEXT_PLATFORM, (cl_context_properties)(_platforms[_platform_used])(), 0};

    _context = cl::Context(CL_DEVICE_TYPE_GPU, properties);

    _all_devices = _context.getInfo<CL_CONTEXT_DEVICES>();
    if (_all_devices.size() == 0) {
        std::cerr << "Could not find any devices of this type." << std::endl << std::endl;
    }

    if(_verbose) printf("number of devices %lu\n", _all_devices.size());
    _device = _all_devices[_device_used];

    // Get some info about the device
    try{
        _phase = "_device.getInfo";

        std::string device_name, device_vendor;
        _err = _device.getInfo<std::string>(CL_DEVICE_VENDOR, &device_vendor); checkError();
        _err = _device.getInfo<std::string>(CL_DEVICE_NAME, &device_name); checkError();
        if(_verbose) printf("Using device \"%s\" from \"%s\"\n", device_name.c_str(), device_vendor.c_str());

        // _err = _device.getInfo(CL_DEVICE_MAX_COMPUTE_UNITS, &compute_units);
        // checkError();
        // printf("CL_DEVICE_MAX_COMPUTE_UNITS = %u\n", (unsigned int)compute_units);

        // _err = _device.getInfo<cl_uint>(CL_DEVICE_MAX_WORK_GROUP_SIZE, &_max_work_group_size);
        // printf("CL_DEVICE_MAX_WORK_GROUP_SIZE = %u\n", (unsigned int)_max_work_group_size);
        _max_work_group_size = 32765;

        _err = _device.getInfo(CL_DEVICE_MAX_WORK_ITEM_SIZES, &_max_work_item_sizes);
        checkError();
        if(_verbose) printf("CL_DEVICE_MAX_WORK_ITEM_SIZES = %lu * %lu * %lu\n", _max_work_item_sizes[0], _max_work_item_sizes[1], _max_work_item_sizes[2]);

        _err = _device.getInfo<cl_ulong>(CL_DEVICE_LOCAL_MEM_SIZE, &_localMemSize);    
        checkError();

        if(_verbose) printf("CL_DEVICE_LOCAL_MEM_SIZE = %lu\n", _localMemSize);

        _err = _device.getInfo<cl_ulong>(CL_DEVICE_GLOBAL_MEM_SIZE, &_globalMemSize);    
        checkError();

        if(_verbose) printf("CL_DEVICE_GLOBAL_MEM_SIZE = %lu\n", _globalMemSize);

        // calculate maximum number of positions that one thread in detector can process
        // (the whole work group needs all pixels in a window around a position of 
        // each thread in the image)
        int workgroupSizeX = 32;
        int workgroupSizeY = 8;
        int windowSize = 16;
        int elemSize = sizeof(uchar)+sizeof(short)+sizeof(short);
        int memoryRequirement;
        _max_positions_per_thread = -1;
        do {
            ++_max_positions_per_thread;
            int numElementsX = (workgroupSizeX * 2) + (windowSize - 1);
            int numElementsY = ( (workgroupSizeY * (_max_positions_per_thread+1)) * 2) + windowSize - 1;
            int elemCount = numElementsX*numElementsY;
            memoryRequirement = elemCount*elemSize;
        } while(memoryRequirement < _localMemSize);

        if(_max_positions_per_thread == 0) {
            std::cerr << "Error: Shared memory size of selected OpenCL device is " << _localMemSize << " B, but the program needs at least " << memoryRequirement << " B.\n";
            throw;
        } 

        if(_verbose) printf("Maximum number of rows per thread = %d\n", _max_positions_per_thread);
        
        
    }
    catch (std::exception e) {
        printf("ERROR: %s(%s)\n", _phase.c_str(), oclErrorString(_err));
        throw;
    }
    
    // Create the command queue we will use to execute OpenCL commands
    try{
        _phase = "cl::CommandQueue";
        _queue = cl::CommandQueue(_context, _device, 0, &_err);
        checkError();
    }
    catch (std::exception e) {
        printf("ERROR: %s(%s)\n", _phase.c_str(), oclErrorString(_err));
        throw;
    }    

    loadProgram(window_width, window_height, feature_limit, fids_limit, tree_depth, qhs_limit, k, q, shrink);
}



CL::~CL()
{

}

/********************* ^ Constructor & Destructor ^ ************************/

/*************************** v Load program v ******************************/

void CL::loadProgram(const int window_width, const int window_height, const int feature_limit, const int fids_limit, const int tree_depth, const int qhs_limit, const float k, const int q, const int shrink)
{
    // Program Setup
    if(_verbose) printf("load the program\n");
    
    _kernel_source_file.open(_kernel_source_path);
    // Read the program source
    std::string kernel_source( std::istreambuf_iterator<char>(_kernel_source_file), (std::istreambuf_iterator<char>()));

    try
    {
        _phase = "cl::Program";
        cl::Program::Sources source(1, std::make_pair(kernel_source.c_str(),
                                                      kernel_source.length()));
        _program = cl::Program(_context, source);
    }
    catch (std::exception e) {
        printf("ERROR: %s(%s)\n", _phase.c_str(), oclErrorString(_err));
    }

    std::stringstream build_options;
    build_options << "-D WINDOW_WIDTH=" << window_width << " -D WINDOW_HEIGHT=" << window_height << " -D FEATURE_LIMIT=" << feature_limit << " -D FIDS_LIMIT=" << fids_limit << " -D TREE_DEPTH=" << tree_depth << " -D QHS_LIMIT=" << qhs_limit << " -D K=" << k << " -D Q=" << q << " -D SHRINK=" << shrink << " -D ROWS_PER_THREAD=" << _max_positions_per_thread;

    if(_verbose) printf("build program\n");
    try
    {
        _phase = "_program.build";        
        _err = _program.build(_all_devices, build_options.str().c_str());
        checkError();
    }
    catch (std::exception e) {
        printf("ERROR: %s(%s)\n", _phase.c_str(), oclErrorString(_err));
    }

    if(_verbose) {
        printf("done building program\n");
        std::cout << "Build Status: " << _program.getBuildInfo<CL_PROGRAM_BUILD_STATUS>(_all_devices[0]) << std::endl;
        std::cout << "Build Options:\t" << _program.getBuildInfo<CL_PROGRAM_BUILD_OPTIONS>(_all_devices[0]) << std::endl;
        std::cout << "Build Log:\t " << _program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_all_devices[0]) << std::endl;
    }
    
    // Initialize our kernels from the program
    try{
        _phase = "cl::Kernel";
        _kernelGS = cl::Kernel(_program, "gaussian_blur", &_err);
        checkError();

        _kernelShrink = cl::Kernel(_program, "shrink", &_err);
        checkError();

        _kernelShrinkUnsigned = cl::Kernel(_program, "shrinkUnsigned", &_err);
        checkError();

        _kernelShrinkSigned = cl::Kernel(_program, "shrinkSigned", &_err);
        checkError();

        _kernelDiffx = cl::Kernel(_program, "diffx", &_err);
        checkError();

        _kernelDiffy = cl::Kernel(_program, "diffy", &_err);
        checkError();

        _kernelResize = cl::Kernel(_program, "resizeImage", &_err);
        checkError();

        _kernelDetect = cl::Kernel(_program, "detect", &_err);
        checkError();

    }
    catch (std::exception e) {
        printf("ERROR: %s(%s)\n", _phase.c_str(), oclErrorString(_err));
        throw;
    }
}

/*************************** ^ Load program ^ ******************************/

/************************* v Getters & setters v ***************************/

unsigned long CL::getGlobalMemSize() { return _globalMemSize; }

/************************* ^ Getters & setters ^ ***************************/

/**************************** v Buffer data v ******************************/


void CL::bufferData(int width, int height, int pyramidWidth, int pyramidHeight, std::vector<int>& fids, std::vector<int>& thrs, std::vector<int>& qhs, float shrink)
{
    size_t num_elements = width*height;


    try{
        // Create input buffer.
        _phase = "ARRAY_BUFFER: cl::Buffer";
        // _cl_input = cl::Buffer(_context, CL_MEM_READ_ONLY, num_elements*sizeof(float), NULL, &_err);
        // checkError();



        _phase = "RESULT_ARRAY_BUFFER: cl::Buffer";
        // Create pyramid buffer for gaussian and fill it with zeros.
        _cl_gaussian_pyramid = cl::Buffer(_context, CL_MEM_READ_WRITE, pyramidWidth*pyramidHeight*sizeof(float), NULL, &_err);
        checkError();
        float pattern = 0;
        _err = _queue.enqueueFillBuffer(_cl_gaussian_pyramid, pattern, 0, pyramidWidth*pyramidHeight*sizeof(float));
        checkError();



        // Create output buffer for diffx result.
        _phase = "RESULT_ARRAY_BUFFER: cl::Buffer";

        // Create pyramid buffer for diffx and fill it with zeros.
        _cl_gaussian_diffx_pyramid = cl::Buffer(_context, CL_MEM_READ_WRITE, pyramidWidth*pyramidHeight*sizeof(float), NULL, &_err);
        checkError();
        pattern = 0;
        _err = _queue.enqueueFillBuffer(_cl_gaussian_diffx_pyramid, pattern, 0, pyramidWidth*pyramidHeight*sizeof(float));
        checkError();


        // Create pyramid buffer for diffx and fill it with zeros.
        _cl_gaussian_diffy_pyramid = cl::Buffer(_context, CL_MEM_READ_WRITE, pyramidWidth*pyramidHeight*sizeof(float), NULL, &_err);
        checkError();
        pattern = 0;
        _err = _queue.enqueueFillBuffer(_cl_gaussian_diffy_pyramid, pattern, 0, pyramidWidth*pyramidHeight*sizeof(float));
        checkError();


        // Create buffer for fids and fill it with data.
        _phase = "RESULT_ARRAY_BUFFER: cl::Buffer";
        _cl_fids = cl::Buffer(_context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, fids.size()*sizeof(int), (void *)fids.data(), &_err);
        checkError();

        // Create buffer for thrs and fill it with data.
        _phase = "RESULT_ARRAY_BUFFER: cl::Buffer";
        _cl_thrs = cl::Buffer(_context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, thrs.size()*sizeof(int), (void *)thrs.data(), &_err);
        checkError();

        // Create buffer for qhs and fill it with data.
        _phase = "RESULT_ARRAY_BUFFER: cl::Buffer";
        _cl_qhs = cl::Buffer(_context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, qhs.size()*sizeof(int), (void *)qhs.data(), &_err);
        checkError();

        _shrink = shrink;


        // Create buffer for output detections (centers of rectangles).
        _phase = "RESULT_ARRAY_BUFFER: cl::Buffer";
        // 2 numbers for each detection, but only every second position is evaluated
        _cl_detections = cl::Buffer(_context, CL_MEM_WRITE_ONLY, pyramidWidth*pyramidHeight*sizeof(int), NULL, &_err); 
        checkError();

        // Create buffer for output qhs.
        _phase = "RESULT_ARRAY_BUFFER: cl::Buffer";
        _cl_out_hs = cl::Buffer(_context, CL_MEM_WRITE_ONLY, pyramidWidth*pyramidHeight*sizeof(int), NULL, &_err);
        checkError();




        // Create buffer for storing number of detections
        _phase = "RESULT_ARRAY_BUFFER: cl::Buffer";
        _cl_num_detections = cl::Buffer(_context, CL_MEM_READ_WRITE, sizeof(int), NULL, &_err);
        checkError();


    }
    catch (std::exception e) {
        printf("ERROR: %s(%s)\n", _phase.c_str(), oclErrorString(_err));
        throw;
    }

    _image_loaded = true;

}

/**************************** ^ Buffer data ^ ******************************/


/************************** v Execute kernels v ****************************/


unsigned int CL::iCeilTo(unsigned int data, unsigned int align_size)
{
    return ((data - 1 + align_size) / align_size) * align_size;
}

int glPyrHeight;




int CL::runKernels(cv::Mat &gray, float scaleRatio, float scaleInitial, int pyramidWidth, int pyramidHeight, std::vector<cv::Point2i> &pyramidCoords, cv::Mat& gausPyramid, std::vector<int> &detections, std::vector<int> &out_hs)
{    
    glPyrHeight = pyramidHeight;

    int width = gray.cols;
    int height = gray.rows;

    //if(_verbose) printf("in runKernel\n");

    try {

        // Copy gaussian kernel result to image.
        cl::size_t<3> origin;
        origin[0] = 0;
        origin[1] = 0;
        origin[2] = 0;

        cl::size_t<3> region;
        region[0] = width;
        region[1] = height;
        region[2] = 1;

        _cl_images_scaled[1] = cl::Image2D(_context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, _float_format, width, height, 0, (void*)gray.data, &_err);

        checkError();


        createPyramids(width, height, _cl_images_scaled, _cl_gaussian_pyramid, _cl_gaussian_diffx_pyramid, _cl_gaussian_diffy_pyramid, scaleRatio, scaleInitial, pyramidCoords, pyramidWidth);

        /*** DETECTION ***/

        int num_detections = 0;
        _phase = "ARRAY_BUFFER: _queue.enqueueWriteBuffer(_cl_num_detections)";
        _err = _queue.enqueueWriteBuffer(_cl_num_detections, CL_TRUE, 0, sizeof(int), (void*) &num_detections, NULL, &_event);
        checkError();


        // Set detection kernel arguments.
        _phase = "_kernelDetect.setArg";
        _err = _kernelDetect.setArg(0, _cl_gaussian_pyramid); checkError();
        _err = _kernelDetect.setArg(1, _cl_gaussian_diffx_pyramid); checkError();
        _err = _kernelDetect.setArg(2, _cl_gaussian_diffy_pyramid); checkError();
        _err = _kernelDetect.setArg(3, pyramidWidth); checkError();
        _err = _kernelDetect.setArg(4, pyramidHeight); checkError();
        _err = _kernelDetect.setArg(5, _cl_fids); checkError();
        _err = _kernelDetect.setArg(6, _cl_thrs); checkError();
        _err = _kernelDetect.setArg(7, _cl_qhs); checkError();
        _err = _kernelDetect.setArg(8, _cl_detections); checkError();
        _err = _kernelDetect.setArg(9, _cl_out_hs); checkError();
        _err = _kernelDetect.setArg(10, _cl_num_detections); checkError();

        // Execture detection kernel.
        _phase = "_queue.enqueueNDRangeKernel1";
        _err = _queue.enqueueNDRangeKernel(_kernelDetect, cl::NullRange, cl::NDRange(iCeilTo(pyramidWidth/2, 32), iCeilTo(pyramidHeight/(2), 8)), cl::NDRange(32, 8), NULL, &_event);
        // _err = _queue.enqueueNDRangeKernel(_kernelDetect, cl::NullRange, cl::NDRange(iCeilTo(pyramidWidth/2, 32), iCeilTo(pyramidHeight/(2*_max_positions_per_thread), 8)), cl::NDRange(32, 8), NULL, &_event);
        checkError();

        // std::cout << "pyr height " << pyramidHeight << "\t wgy " << iCeilTo(pyramidHeight/(2*_max_positions_per_thread), 8) << "\n";

        _phase = "output: _queue.enqueueReadBuffer _cl_num_detections";
        _err = _queue.enqueueReadBuffer(_cl_num_detections, CL_TRUE, 0, sizeof(int), (void*) &num_detections, NULL, &_event);
        checkError();

        detections.resize(2*num_detections);
        out_hs.resize(num_detections);
        if(num_detections > 0) {

            _phase = "output: _queue.enqueueReadBuffer _cl_detections";
            _err = _queue.enqueueReadBuffer(_cl_detections, CL_TRUE, 0, 2*num_detections*sizeof(int), (void*) detections.data(), NULL, &_event);
            checkError();
            
            _phase = "output: _queue.enqueueReadBuffer _cl_out_hs";
            _err = _queue.enqueueReadBuffer(_cl_out_hs, CL_TRUE, 0, num_detections*sizeof(int), (void*) out_hs.data(), NULL, &_event);
            checkError();

        }
    
        std::cout << "Num_det: " << num_detections << "\n";


        return num_detections;

        /*************************/


    }
    catch (std::exception e) {
        printf("ERROR: %s(%s)\n", _phase.c_str(), oclErrorString(_err));
        throw;
    }

    return 0;
}


/************************** ^ Execute kernels ^ ****************************/


/************************** v Create pyramids v ****************************/

void CL::createPyramids(int width, int height, std::vector<cl::Image2D> &scaledImages, cl::Buffer &gaussPyramid, cl::Buffer &diffxPyramid, cl::Buffer &diffyPyramid, float scaleRatio, float scaleInitial, std::vector<cv::Point2i> &pyramidCoords, int pyramidWidth)
{
    // Index od the image in scaledImages that should have the 
    // right scale in current iteration. The very first image
    // should therefore be in scaledImages[0] before entering 
    // this function.

    int scaled_img_i = 0;  

    float current_scale = 1.0;
    float resize_scale = scaleInitial;
    width *= scaleInitial;
    height *= scaleInitial;

    for (int i = 0; i < pyramidCoords.size(); ++i) {
        
        // Resize
        scaledImages[scaled_img_i] = cl::Image2D(_context, CL_MEM_READ_WRITE, _float_format, width, height, 0, NULL, &_err);
        checkError();

        _phase = "_kernelResize.setArg";
        _err = _kernelResize.setArg(0, scaledImages[(scaled_img_i+1)%2]); checkError();
        _err = _kernelResize.setArg(1, scaledImages[scaled_img_i]); checkError();
        _err = _kernelResize.setArg(2, resize_scale); checkError();

        // execute the kernel
        _phase = "_queue.enqueueNDRangeKernel(_kernelResize)";
        _err = _queue.enqueueNDRangeKernel(_kernelResize, cl::NullRange, cl::NDRange(iCeilTo((int)(width), 16), iCeilTo((int)(height), 16)), cl::NDRange(16, 16), NULL, &_event);
        checkError();

        // Blur
        _cl_image_blurred = cl::Image2D(_context, CL_MEM_READ_WRITE, _float_format, width, height, 0, NULL, &_err);
        checkError();

        _phase = "_kernelGS.setArg";
        _err = _kernelGS.setArg(0, scaledImages[scaled_img_i]); checkError();
        _err = _kernelGS.setArg(1, _cl_image_blurred); checkError();

        _phase = "_queue.enqueueNDRangeKernel(_kernelGS)";
        _err = _queue.enqueueNDRangeKernel(_kernelGS, cl::NullRange, cl::NDRange(iCeilTo((int)(width), 16), iCeilTo((int)(height), 16)), cl::NDRange(16, 16), NULL, &_event);
        checkError();

        // diffx
        _cl_image_diffx = cl::Image2D(_context, CL_MEM_READ_WRITE, cl::ImageFormat(CL_R, CL_FLOAT), width, height, 0, NULL, &_err);
        checkError();

        _phase = "_kernelDiffx.setArg";
        _err = _kernelDiffx.setArg(0, _cl_image_blurred); checkError();
        _err = _kernelDiffx.setArg(1, _cl_image_diffx); checkError();

        _phase = "_queue.enqueueNDRangeKernel(_kernelDiffx)";
        _err = _queue.enqueueNDRangeKernel(_kernelDiffx, cl::NullRange, cl::NDRange(iCeilTo((int)(width), 16), iCeilTo((int)(height), 16)), cl::NDRange(16, 16), NULL, &_event);
        checkError();



        // cl::size_t<3> origin;
        // origin[0] = 0;
        // origin[1] = 0;
        // origin[2] = 0;

        // cl::size_t<3> region;
        // region[0] = width;
        // region[1] = height;
        // region[2] = 1;

        // cv::Mat image_(height, width, CV_32F);
        // _err = _queue.enqueueReadImage(_cl_image_diffx, CL_TRUE, origin, region, 0, 0, (void*)image_.data, NULL, &_event);
        // checkError();
        // cv::normalize(image_, image_, 0, 1, cv::NORM_MINMAX, CV_32F);
        // cv::imshow("try", image_);
        // cv::waitKey();
        // // cv::destroyWindow("try");



        // diffy
        _cl_image_diffy = cl::Image2D(_context, CL_MEM_READ_WRITE, cl::ImageFormat(CL_R, CL_FLOAT), width, height, 0, NULL, &_err);
        checkError();

        _phase = "_kernelDiffy.setArg";
        _err = _kernelDiffy.setArg(0, _cl_image_blurred); checkError();
        _err = _kernelDiffy.setArg(1, _cl_image_diffy); checkError();

        _phase = "_queue.enqueueNDRangeKernel(_kernelDiffy)";
        _err = _queue.enqueueNDRangeKernel(_kernelDiffy, cl::NullRange, cl::NDRange(iCeilTo((int)(width), 16), iCeilTo((int)(height), 16)), cl::NDRange(16, 16), NULL, &_event);
        checkError();


        // Shrink all and store in pyramids:
        // shrink blurred image
        _phase = "_kernelShrink.setArg";
        _phase = "_kernelShrinkUnsigned.setArg";
        _err = _kernelShrink.setArg(0, _cl_image_blurred); checkError();
        _err = _kernelShrink.setArg(1, gaussPyramid); checkError();
        _err = _kernelShrink.setArg(2, pyramidCoords[i].x); checkError();
        _err = _kernelShrink.setArg(3, pyramidCoords[i].y); checkError();
        _err = _kernelShrink.setArg(4, pyramidWidth); checkError();

        _phase = "_queue.enqueueNDRangeKernel(_kernelShrinkUnsigned)";
        _err = _queue.enqueueNDRangeKernel(_kernelShrink, cl::NullRange, cl::NDRange(iCeilTo((int)(width), 16), iCeilTo((int)(height), 16)), cl::NDRange(16, 16), NULL, &_event);
        checkError();

        // shrink diffx image
        _phase = "_kernelShrinkSigned.setArg";
        _err = _kernelShrink.setArg(0, _cl_image_diffx); checkError();
        _err = _kernelShrink.setArg(1, diffxPyramid); checkError();
        _err = _kernelShrink.setArg(2, pyramidCoords[i].x); checkError();
        _err = _kernelShrink.setArg(3, pyramidCoords[i].y); checkError();
        _err = _kernelShrink.setArg(4, pyramidWidth); checkError();

        _phase = "_queue.enqueueNDRangeKernel(_kernelShrinkSigned)";
        _err = _queue.enqueueNDRangeKernel(_kernelShrink, cl::NullRange, cl::NDRange(iCeilTo((int)(width), 16), iCeilTo((int)(height), 16)), cl::NDRange(16, 16), NULL, &_event);
        checkError();


        // shrink diffy image
        _phase = "_kernelShrinkSigned.setArg";
        _err = _kernelShrink.setArg(0, _cl_image_diffy); checkError();
        _err = _kernelShrink.setArg(1, diffyPyramid); checkError();

        _phase = "_queue.enqueueNDRangeKernel(_kernelShrinkSigned)";
        _err = _queue.enqueueNDRangeKernel(_kernelShrink, cl::NullRange, cl::NDRange(iCeilTo(ceil((float)width/_shrink), 16), iCeilTo(ceil((float)height/_shrink), 16)), cl::NDRange(16, 16), NULL, &_event);
        checkError();

        // cv::Mat out = cv::Mat::zeros(glPyrHeight, pyramidWidth, CV_32F);
        // _err = _queue.enqueueReadBuffer(diffxPyramid, CL_TRUE, 0, pyramidWidth*glPyrHeight*sizeof(float), (void*)out.data, NULL, &_event);
        // checkError();
        // // float resize_scale_w = 1750.0f/(float)out.cols;
        // // float resize_scale_h = 880.0f/(float)out.rows;
        // // float result_scale = std::min<float>(resize_scale_h, resize_scale_w);
        // // resize(out, out, cv::Size(), result_scale, result_scale, cv::INTER_NEAREST);
        // cv::normalize(out, out, 0, 1, cv::NORM_MINMAX, CV_32F);
        // cv::imshow("try", out);
        // cv::waitKey();
        // // cv::destroyWindow("try");

        
        // finish iteration
        width = (float)width*scaleRatio;
        height = (float)height*scaleRatio;
        current_scale *= scaleRatio;
        resize_scale = scaleRatio;
        scaled_img_i = (scaled_img_i+1)%2;
    } // for
}

/************************** ^ Create pyramids ^ ****************************/











/************************** v Create pyramids old v ****************************/

// void CL::createPyramids(int width, int height, std::vector<cl::Image2D> &inputImages, cl::Buffer &outputBuffer, float scaleRatio, std::vector<cv::Point2i> &pyramidCoords, int pyramidWidth)
// {
//     int src_i = 0;
//     float kernel_scale = 1.0;
//     // float shrink = 1.0;
//     // float scaleRatioInv = 1.0/scaleRatio;

//     // Create second image texture for gaussian.
//     inputImages[1] = cl::Image2D(_context, CL_MEM_READ_WRITE, cl::ImageFormat(CL_R, CL_FLOAT), width, height, 0, NULL, &_err);
//     checkError();

//     cl::Kernel *selected_kernel = &_kernelResizeBlur;
//     float current_scale = 1.0;
//     float next_scale_thresh = 0.3332;

//     _phase = "_kernelResize.setArg";
//     _err = _kernelResize.setArg(6, pyramidWidth); checkError();
//     _err = _kernelResizeBlur.setArg(6, pyramidWidth); checkError();

//     for (int i = 0; i < pyramidCoords.size(); ++i) {

//         selected_kernel = &_kernelResizeBlur;

//         if(current_scale < next_scale_thresh) {
//             selected_kernel = &_kernelResizeBlur;
//             next_scale_thresh = current_scale/2;
//         }
        
//         _phase = "_kernelResize.setArg";
//         _err = selected_kernel->setArg(0, inputImages[src_i]); checkError();
//         _err = selected_kernel->setArg(1, inputImages[(src_i+1)%2]); checkError();
//         _err = selected_kernel->setArg(2, outputBuffer); checkError();
//         _err = selected_kernel->setArg(3, kernel_scale); checkError();
//         _err = selected_kernel->setArg(4, pyramidCoords[i].x); checkError();
//         _err = selected_kernel->setArg(5, pyramidCoords[i].y); checkError();


//         // execute the kernel
//         _phase = "_queue.enqueueNDRangeKernel(_kernelResize)";
//         _err = _queue.enqueueNDRangeKernel(*selected_kernel, cl::NullRange, cl::NDRange(iCeilTo((int)(width), 16), iCeilTo((int)(height), 16)), cl::NDRange(16, 16), NULL, &_event);
//         checkError();

//         width = (float)width*scaleRatio;
//         height = (float)height*scaleRatio;
//         inputImages[src_i] = cl::Image2D(_context, CL_MEM_READ_WRITE, cl::ImageFormat(CL_R, CL_FLOAT), width, height, 0, NULL, &_err);
//         checkError();

//         // shrink = scaleRatioInv;
//         kernel_scale = scaleRatio;
//         current_scale *= scaleRatio;
//         src_i = (src_i+1)%2;
//     }
// }

/************************** ^ Create pyramids old ^ ****************************/




// *************** Debug code for viewing image in a buffer
// cv::Mat outGaussPyramid = cv::Mat::zeros(glPyrHeight, pyramidWidth, CV_32F);
// _phase = "output: _queue.enqueueReadBuffer _cl_gaussian_pyramid";
// _err = _queue.enqueueReadBuffer(outputBuffer, CL_TRUE, 0, pyramidWidth*glPyrHeight*sizeof(float), (void*)outGaussPyramid.data, NULL, &_event);
// checkError();
// float resize_scale_w = 1750.0f/(float)outGaussPyramid.cols;
// float resize_scale_h = 880.0f/(float)outGaussPyramid.rows;
// float result_scale = std::min<float>(resize_scale_h, resize_scale_w);
// resize(outGaussPyramid, outGaussPyramid, cv::Size(), result_scale, result_scale, cv::INTER_NEAREST);
// cv::normalize(outGaussPyramid, outGaussPyramid, 0, 1, cv::NORM_MINMAX, CV_32F);
// cv::imshow("try", outGaussPyramid);
// cv::waitKey();
// cv::destroyWindow("try");


// *************** Debug code for viewing image in a Image2D buffer
// cl::size_t<3> origin;
// origin[0] = 0;
// origin[1] = 0;
// origin[2] = 0;

// cl::size_t<3> region;
// region[0] = width;
// region[1] = height;
// region[2] = 1;

// cv::Mat image_(height, width, CV_32F);
// _phase = "output: _queue.enqueueReadBuffer _cl_gaussian_pyramid";
// _err = _queue.enqueueReadImage(inputImages[(src_i+1)%2], CL_TRUE, origin, region, 0, 0, (void*)image_.data, NULL, &_event);
// checkError();
// cv::normalize(image_, image_, 0, 1, cv::NORM_MINMAX, CV_32F);
// cv::imshow("try", image_);
// cv::waitKey();
// cv::destroyWindow("try");









// Mat m(chan[1].height, chan[1].width, CV_32F);
// for(int y = 0;y < chan[1].height;y++){
//     for(int x = 0;x < chan[1].width;x++){
//         m.at<float>(y,x) = chan[1].at(y,x);
//     }
// }

// cv::normalize(m, m, 0, 1, cv::NORM_MINMAX, CV_32F);
// cv::imshow("try2", m);
// cv::waitKey();
// cv::destroyWindow("try2");
