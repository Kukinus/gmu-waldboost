__constant sampler_t samplerIn = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_LINEAR;


__kernel void resizeImage(
                          __read_only  image2d_t sourceImage,
                          __write_only  image2d_t targetImage,
                          float scale
                          )
{
    int global_x = get_global_id(0);
    int global_y = get_global_id(1);

    int scaledWidth = (int) (get_image_width(sourceImage)*scale);
    int scaledHeight = (int) (get_image_height(sourceImage)*scale);

    if(global_x < scaledWidth && global_y < scaledHeight){

        float2 posIn = (float2) (global_x/scale + 0.5, global_y/scale + 0.5);
    
        float4 pixel = read_imagef(sourceImage, samplerIn, posIn);
        write_imagef(targetImage, (int2)(global_x, global_y), pixel);
    }
}


#define FILTER_HALF_SIZE 1
#define FILTER_SIZE (2*FILTER_HALF_SIZE+1)

__kernel void gaussian_blur(
        __read_only  image2d_t inputImage,
        __write_only image2d_t blurredImage
    ) {

    int global_x = (int)get_global_id(0);
    int global_y = (int)get_global_id(1);

    int width = get_image_width(inputImage);
    int height = get_image_height(inputImage);

    
    if(global_x < width && global_y < height){

        float filter[3][3] = {{ 1,2,1},{2,4,2},{1,2,1}};

        

        float4 sum = (float4)(0.0,0.0,0.0,0.0);

        float srcposx = (global_x + 0.5);
        float srcposy = (global_y + 0.5);
        for(int yy = -FILTER_HALF_SIZE;yy <= FILTER_HALF_SIZE; yy++){

            float posy = srcposy+yy;
            for(int xx = -FILTER_HALF_SIZE;xx <= FILTER_HALF_SIZE; xx++){
                float2 posIn = (float2) (srcposx+xx, posy);
                //read_imagef(inputImage, samplerIn, posIn);

                sum += read_imagef(inputImage, samplerIn, posIn) * filter[yy+FILTER_HALF_SIZE][xx+FILTER_HALF_SIZE];
            }

        }

        write_imagef(blurredImage, (int2)(srcposx, srcposy), sum/16);
    }

}

// define SHRINK is passed to program by host compiler
#define SHRINK2 (SHRINK*SHRINK)

__kernel void shrink(
                          __read_only  image2d_t inputImage,
                          __global float * imagePyramid,
                          int xOffset,
                          int yOffset,
                          int imagePyramidWidth)
{
    int global_x = (int)get_global_id(0);
    int global_y = (int)get_global_id(1);

    int width = get_image_width(inputImage)/SHRINK;
    int height = get_image_height(inputImage)/SHRINK;

    float4 out;

    if(global_x < width && global_y < height){

        out = 0.0;

        int srcposx = global_x*SHRINK;
        int srcposy = global_y*SHRINK;

        for(int yy = 0; yy < SHRINK; yy++){

            int posy = srcposy+yy;
            for(int xx = 0; xx < SHRINK; xx++){

                out  += read_imagef(inputImage, samplerIn, (float2) (srcposx+xx, posy));

            }

        }
                    
        out /= SHRINK2;

        imagePyramid[(global_y + yOffset) * imagePyramidWidth + global_x + xOffset] = out.x;

    }
}



__kernel void shrinkUnsigned(
                          __read_only  image2d_t inputImage,
                          __global uchar * imagePyramid,
                          int xOffset,
                          int yOffset,
                          int imagePyramidWidth)
{
    int global_x = (int)get_global_id(0);
    int global_y = (int)get_global_id(1);

    int width = get_image_width(inputImage)/SHRINK;
    int height = get_image_height(inputImage)/SHRINK;

    if(global_x < width && global_y < height){

        float4 out = (float4) (0.0,0.0,0.0,0.0);

        int srcposx = global_x*SHRINK;
        int srcposy = global_y*SHRINK;

        for(int yy = 0; yy < SHRINK; yy++){

            int posy = srcposy+yy;
            for(int xx = 0; xx < SHRINK; xx++){

                out  += read_imagef(inputImage, samplerIn, (int2) (srcposx+xx, posy));

            }

        }
                    
        out /= SHRINK2;

        // if (global_x == 100 && (global_y > 100 && global_y < 120)) printf("y %d before conv: %g\n", global_y, out.x);

        imagePyramid[(global_y + yOffset) * imagePyramidWidth + global_x + xOffset] = convert_uchar_sat_rtn(out.x);

        // if (global_x == 100 && (global_y > 100 && global_y < 120)) printf("y %d after conv: %d\n", global_y, convert_uchar_sat_rtn(out.x));

    }
}



__kernel void shrinkSigned(
                          __read_only  image2d_t inputImage,
                          __global short * imagePyramid,
                          int xOffset,
                          int yOffset,
                          int imagePyramidWidth)
{
    int global_x = (int)get_global_id(0);
    int global_y = (int)get_global_id(1);

    int width = get_image_width(inputImage)/SHRINK;
    int height = get_image_height(inputImage)/SHRINK;

    if(global_x < width && global_y < height){

        float4 out = (float4) (0.0,0.0,0.0,0.0);

        int srcposx = global_x*SHRINK;
        int srcposy = global_y*SHRINK;

        for(int yy = 0; yy < SHRINK; yy++){

            int posy = srcposy+yy;
            for(int xx = 0; xx < SHRINK; xx++){

                out  += read_imagef(inputImage, samplerIn, (int2) (srcposx+xx, posy));

            }

        }
                    
        out /= SHRINK2;

        imagePyramid[(global_y + yOffset) * imagePyramidWidth + global_x + xOffset] = convert_short_sat_rtn(out.x);

    }
}





__kernel void diffx(
        __read_only  image2d_t inputImage,
        __write_only image2d_t diffedImage
    ) {

    int global_x = (int)get_global_id(0);
    int global_y = (int)get_global_id(1);

    int width =  get_image_width(inputImage);
    int height = get_image_height(inputImage);

    if(global_x < width && global_y < height){

        float4 pix1 = read_imagef(inputImage, samplerIn, (int2)(global_x - 1, global_y));
        float4 pix2 = read_imagef(inputImage, samplerIn, (int2)(global_x + 1, global_y));

        float4 outPix = (pix2 - pix1)/2;
        write_imagef(diffedImage, (int2)(global_x, global_y), outPix);
    }

}

__kernel void diffy(
        __read_only  image2d_t inputImage,
        __write_only  image2d_t diffedImage
    ) {

    int global_x = (int)get_global_id(0);
    int global_y = (int)get_global_id(1);

    int width =  get_image_width(inputImage);
    int height = get_image_height(inputImage);

    if(global_x < width && global_y < height){

        float4 pix1 = read_imagef(inputImage, samplerIn, (int2)(global_x, global_y - 1));
        float4 pix2 = read_imagef(inputImage, samplerIn, (int2)(global_x, global_y + 1));

        float4 outPix = (pix2 - pix1)/2;
        write_imagef(diffedImage, (int2)(global_x, global_y), outPix);
    }

}

// // These defines are given by the host program during compilation:
// #define WINDOW_WIDTH 64
// #define WINDOW_HEIGHT 64
// #define FEATURE_LIMIT 1500
// #define FIDS_LIMIT 3
// #define TREE_DEPTH 2
// #define QHS_LIMIT 4
// #define K 0.5
// #define Q -1024

#define SHRUNK_WINDOW_WIDTH (WINDOW_WIDTH/SHRINK)
#define SHRUNK_WINDOW_HEIGHT (WINDOW_HEIGHT/SHRINK)


__kernel void detect (
        __global float * pyr,
        __global float * dxpyr,
        __global float * dypyr,
        int pyr_width,
        int pyr_height,
        __constant int * fids,
        __constant int * thrs,
        __constant int * qhs,
        __global int2 * detections,
        __global int * out_hs,
        volatile __global int * det_i
    ) {

    // process only every second candidate position
    int candidate_x = get_global_id(0) * 2;
    int start_y = (get_global_id(1) * 2 * ROWS_PER_THREAD);
    int candidate_y = get_global_id(1) * 2;

    int local_h = (int)get_local_size(1);

    // int candidate_y;
    // int pyr_pos_i = 0;
    // for(pyr_pos_i = 0; pyr_pos_i < ROWS_PER_THREAD; ++pyr_pos_i) {

    //     candidate_y = start_y + (2*pyr_pos_i);

    //     if(candidate_y >= pyr_height) break;

        int suma = 0;
        #pragma unroll 
        for(int i = 0; i < FEATURE_LIMIT; i++){
            int node = 0;
            int treeAddr = i*FIDS_LIMIT;
            #pragma unroll 
            for(int l = 0; l < TREE_DEPTH; l++) {
                int pos = fids[treeAddr+ node];
                int th = thrs[treeAddr+ node];
                int chan_c;
                int x_c;
                int y_c;

                x_c = ((int)(pos / SHRUNK_WINDOW_WIDTH) % SHRUNK_WINDOW_HEIGHT);
                y_c = (pos % SHRUNK_WINDOW_WIDTH);

                // float val;

                // chan_c = pos/(SHRUNK_WINDOW_WIDTH*SHRUNK_WINDOW_HEIGHT);
                // int extract_pos = (candidate_y+y_c)*pyr_width + candidate_x+x_c;
                // if(chan_c == 0) val = pyr[extract_pos];
                // if(chan_c == 1) val = dxpyr[extract_pos];
                // if(chan_c == 2) val = dypyr[extract_pos];

                float fval;

                chan_c = pos/(SHRUNK_WINDOW_WIDTH*SHRUNK_WINDOW_HEIGHT);
                int extract_pos = (candidate_y+y_c)*pyr_width + candidate_x+x_c;
                if(chan_c == 0) fval = pyr[extract_pos];
                if(chan_c == 1) fval = dxpyr[extract_pos];
                if(chan_c == 2) fval = dypyr[extract_pos];

                short sval = convert_short_sat_rtn(fval);
                
                // if(candidate_x == 100 && candidate_y == 100 && chan_c==0 && l == 0)
                //     printf("OCL chan_c: %d; val: %d\n", chan_c, sval);

                if(sval < th){
                    node = node*2+1;
                }
                else{
                    node = node*2+2;
                }
            } // for TREE_DEPTH

            int qhs_i = i*QHS_LIMIT+ (node-3);
            int hs = qhs[qhs_i];
            suma+= hs;

            if(suma < (K*i+Q))
                break;
            if(i == FEATURE_LIMIT-1){
                int det_i_current = atomic_inc(det_i);
                detections[det_i_current] = (int2)(candidate_x+SHRUNK_WINDOW_WIDTH/2, candidate_y+SHRUNK_WINDOW_HEIGHT/2);
                //printf("CL: x %d\t y %d\t score %d\n", candidate_x+SHRUNK_WINDOW_WIDTH/2, candidate_y+SHRUNK_WINDOW_HEIGHT/2, suma);
                out_hs[det_i_current] = suma;
            }

        } // for FEATURE_LIMIT
    //} // for POSITIONS_PER_THREAD
}



/*__kernel void gaussian_blur_local(
        int width,
        int height,
        __global float * inputImage,
        __global float * blurredImage,
        __local float * tmpLocal
    ) {

    int global_x = (int)get_global_id(0);
    int global_y = (int)get_global_id(1);
    int local_x = (int)get_local_id(0);
    int local_y = (int)get_local_id(1);
    int local_w = (int)get_local_size(0);
    int local_h = (int)get_local_size(1);

    if(global_x < width && global_y < height){


        tmpLocal[local_x + local_y * local_w] = inputImage[global_x + global_y * width];
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    if(global_x < width && global_y < height){


        blurredImage[width*global_y + global_x] = tmpLocal[local_x + local_y * local_w];
    }

}*/

