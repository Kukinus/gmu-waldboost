
// Project libraries.
#include "face_detector_gpu.h"

using namespace std;
using namespace cv;

// Constructor.
FaceDetectorGPU::FaceDetectorGPU(const string & path) :
    FaceDetector(path),
    _clApi(NULL),
    _pyramid_width(-1), _pyramid_height(-1), _max_pyramid_width(-1),
    _img_width(-1), _img_height(-1)
{

}

FaceDetectorGPU::~FaceDetectorGPU() {
    if(_clApi != NULL)
        delete _clApi;
}


bool FaceDetectorGPU::init()
{
    bool success = FaceDetector::init();
    if (success) {
        if(_clApi == NULL)
            _clApi = new CL(_path, WINDOW_WIDTH, WINDOW_HEIGHT, FEATURE_LIMIT, FIDS_LIMIT, TREE_DEPTH, QHS_LIMIT, K, Q, _shrink);
    }
    return success;
}


// Detects face occurences in the image.
void FaceDetectorGPU::compute(const Mat &image, std::vector<Rect> &rects) {

    Mat gray;
    image.convertTo(gray,CV_32F);
    int num_detections;
    std::vector<int> out_hs;
    std::vector<int> detections;
    std::vector<Rect> rects_temp;
    std::vector<int> out_hs_temp;

    if(image.cols != _img_width || image.rows != _img_height) {
        _img_width = image.cols;
        _img_height = image.rows;
        calculatePyramidLayout(_img_width, _img_height, _pyramid_width, _pyramid_height, _pyr_coords);
        _clApi->bufferData(gray.cols, gray.rows, _pyramid_width, _pyramid_height, _fids, _thrs, _qhs, _shrink);
    }

    // showPyramidLayout(gray);

    /*** OPENCL START ***/    

    cv::Mat origPyramid;

    {
        StopWatch spwtch("Image filtration", StopWatch::MS);

        num_detections = _clApi->runKernels(gray, SCALE_RATIO, SCALE_INITIAL, _pyramid_width, _pyramid_height, _pyr_coords, origPyramid, detections, out_hs);
    }

    /*cv::normalize(filteredImg, filteredImg, 0, 1, NORM_MINMAX, CV_32F);
    float resize_scale_w = 1750.0f/(float)filteredImg.cols;
    float resize_scale_h = 880.0f/(float)filteredImg.rows;
    float result_scale = std::min<float>(resize_scale_h, resize_scale_w);
    resize(filteredImg, filteredImg, Size(), result_scale, result_scale, INTER_NEAREST);*/

    //rectss = GenerateResultRectanglesPyr(detectionsBig.data, gray.cols, gray.rows);
    // rects = GenerateResultRectanglesPyr(detections);
    rects = GenerateResultRectanglesDom(detections, gray.cols, gray.rows);
    
    // for(int j= 0; j<rects.size(); ++j) {
    //     cout<<"y: "<<rects[j].y<<" x: "<<rects[j].x<<" suma: "<<out_hs[j]<<endl;
    // }

    rects_temp = rects;
    out_hs_temp = out_hs;
    nmsMaxLinearImproved(rects_temp, out_hs_temp);
    rects = rects_temp;

    // cout << "\n\n";

    // for(int j= 0; j<rects.size(); ++j) {
    //     cout<<"y: "<<rects[j].y<<" x: "<<rects[j].x<<" suma: "<<out_hs_temp[j]<<endl;
    // }

    cout << "Number of detections after NMS: " << rects.size() << endl;

    // cv::normalize(gray, gray, 0, 1, NORM_MINMAX, CV_32F);
    // cv::normalize(origPyramid, origPyramid, 0, 1, NORM_MINMAX, CV_32F);

    // for (int i = 0; i < rects.size(); ++i) {
    //     cv::rectangle(origPyramid, rects[i], cv::Scalar(255,255,255));
    // }

    // for (int i = 0; i < rects.size(); ++i) {
    //     cv::rectangle(gray, rects[i], cv::Scalar(255,255,255));
    // }

    // float resize_scale_w = 1750.0f/(float)gray.cols;
    // float resize_scale_h = 880.0f/(float)gray.rows;
    // float result_scale = std::min<float>(resize_scale_h, resize_scale_w);
    // resize(gray, gray, Size(), result_scale, result_scale, INTER_LINEAR);
    // imshow("tmp", gray);
    // waitKey(0);

    // cout << "===== Scores =====\n";
    // for(int j = 0; j < num_detections; ++j) {
    //     cout << out_hs[j] << "\n";
    // }

    /*** OPENCL END ***/

    // draw all detection
    //for(int i = 0; i < rects.size(); i++) rectangle(draw_image, rects[i], Scalar(255.0f * out_hs[i] / max_suma, 255.0f * out_hs[i] / max_suma, 255.0f * out_hs[i] / max_suma));

    

    //printf("%d  %d  %f  %d\n",stat_pos,stat_feature, stat_feature/(stat_pos*1.0), stat_detect);
}


void FaceDetectorGPU::showPyramidLayout(const Mat &img) {
    
    if(_pyr_coords.size() == 0) {
        throw std::runtime_error("Error: buildPyramids() was called before member variables were initialized with calculatePyramidLayout().");
    }

    cout << "pyramid width: " << _pyramid_width << "\t pyramid height: " << _pyramid_height << "\n";
    Mat pyramid(_pyramid_height, _pyramid_width, CV_8UC1, Scalar(255));
    float current_scale = SCALE_INITIAL;
    std::vector<cv::Point2i>::iterator it = _pyr_coords.begin();
    //cout << "Points:\n";
    RNG rng(12345);
    for(; it != _pyr_coords.end(); ++it) {
        // cout << *it << '\n';
        cv::Point2i opposite_corner(it->x + ceil(img.cols*current_scale)/_shrink, it->y + ceil(img.rows*current_scale)/_shrink);
        Scalar color = Scalar(rng.uniform(0,200), rng.uniform(0, 200), rng.uniform(0, 200));
        cv::rectangle(pyramid, *it, opposite_corner, color, CV_FILLED);
        current_scale *= SCALE_RATIO;
    }

    float resize_scale_w = 1750.0f/(float)pyramid.cols;
    float resize_scale_h = 880.0f/(float)pyramid.rows;
    float result_scale = std::min<float>(resize_scale_h, resize_scale_w);
    resize(pyramid, pyramid, Size(), result_scale, result_scale, INTER_NEAREST);
    imshow("Pyramid", pyramid);
    waitKey();
    destroyWindow("Pyramid");

    return;
}




std::vector<cv::Rect> FaceDetectorGPU::GenerateResultRectanglesPyr(std::vector<int> &centers) {

    std::vector<cv::Rect> detectionRects;

    for (int i = 0; i < centers.size(); i += 2) {

        int detection_x = centers[i];
        int detection_y = centers[i+1];


        int rectWidth = (int) (WINDOW_WIDTH);
        int rectHeight = (int) (WINDOW_HEIGHT);

        detectionRects.push_back(cv::Rect(detection_x - rectWidth/2, detection_y - rectHeight/2, rectWidth, rectHeight));

    }

    return detectionRects;
}


std::vector<cv::Rect> FaceDetectorGPU::GenerateResultRectanglesDom(std::vector<int> &centers, int width, int height) {

    std::vector<cv::Rect> detectionRects;

    for (int i = 0; i < centers.size(); i += 2) {

        int detection_x = centers[i];
        int detection_y = centers[i+1];

        int relative_x = -1;
        int relative_y = -1;

        float currentScale = SCALE_INITIAL;
        for (int coord = 0; coord < _pyr_coords.size(); ++coord) {

            float scaledWidth = ((float)width*currentScale)/(float)_shrink;
            float scaledHeight = ((float)height*currentScale)/(float)_shrink;
            if(
                    detection_x - _pyr_coords[coord].x >= 0 && (float)(detection_x - _pyr_coords[coord].x) < scaledWidth &&
                    detection_y - _pyr_coords[coord].y >= 0 && (float)(detection_y - _pyr_coords[coord].y) < scaledHeight
                    ){
                relative_x = detection_x - _pyr_coords[coord].x;
                relative_y = detection_y - _pyr_coords[coord].y;
                break;
            }

            currentScale *= SCALE_RATIO;
        }

        if(relative_x < 0) continue;


        float x = ((float)(relative_x*_shrink)/currentScale);
        float y = ((float)(relative_y*_shrink)/currentScale);

        float rectWidth = ((float)WINDOW_WIDTH/currentScale);
        float rectHeight = ((float)WINDOW_HEIGHT/currentScale);

        // cout << "center: x:" << x << "\ty: " << y << "\twidth: " << rectWidth << "\theight: " << rectHeight << "\tscale: " << currentScale << endl;

        detectionRects.push_back(cv::Rect(x - rectWidth/2, y - rectHeight/2, rectWidth, rectHeight));
        // cout << "pushed rect: " << cv::Rect(x - rectWidth/2, y - rectHeight/2, rectWidth, rectHeight) << endl;

    }

    return detectionRects;
}




int FaceDetectorGPU::calculateMaxPyramidWidth(int img_width, int img_height) {
    
    unsigned long img_pix_count = (img_width/_shrink)*(img_height/_shrink);

    double OCL_device_max_memory = _clApi->getGlobalMemSize();

    double reserveRatio = 0.8;
    double usable_memory = OCL_device_max_memory*reserveRatio; // leave a general reserve
    usable_memory -= (4*img_pix_count*sizeof(float)); // textures
    usable_memory -= _fids.size()*sizeof(int) + _qhs.size()*sizeof(int) + _thrs.size()*sizeof(int);


    if(usable_memory < 0) {
        string errmsg("Error: selected OpenCL device does not have enought global memory to process such a large image.\n");
        cerr << errmsg;
        throw std::runtime_error(errmsg);
        return 0;
    }

    int sum_of_all_four_pyramids_element_sizes = 3*sizeof(float) + sizeof(int);
    double element_count_per_pyramid = usable_memory/sum_of_all_four_pyramids_element_sizes;

    int one_img_height_with_padding = img_height/_shrink + 16; 

    return element_count_per_pyramid/one_img_height_with_padding;
}



void FaceDetectorGPU::calculatePyramidLayout(int img_start_width,     int img_start_height, 
                                             int &min_pyramid_width, int &pyramid_height,
                                             std::vector<cv::Point2i> &pyr_coords)
{
    int SHRUNK_WINDOW_WIDTH = WINDOW_WIDTH/_shrink;
    int SHRUNK_WINDOW_HEIGHT = WINDOW_HEIGHT/_shrink;

    img_start_width *= SCALE_INITIAL;
    img_start_height *= SCALE_INITIAL;
    
    _max_pyramid_width = calculateMaxPyramidWidth(img_start_width, img_start_height);

    int pyramid_width = 2*(img_start_width/_shrink);
    if (pyramid_width+SHRUNK_WINDOW_WIDTH > _max_pyramid_width) {
        pyramid_width = (img_start_width/_shrink);
    }

    if(_max_pyramid_width < (img_start_width/_shrink)+SHRUNK_WINDOW_WIDTH) {
        string errmsg("Error: image of width " + std::to_string(img_start_width) + 
            " can't fit into pyramid of width " + std::to_string(_max_pyramid_width) + "\n");
        cerr << errmsg;
        throw std::runtime_error(errmsg);
        return;
    }



    int shorter_side = (img_start_width > img_start_height) ? img_start_height : img_start_width;

    float smallest_scale = (float)WINDOW_WIDTH/(float)shorter_side;

    int num_stages = 0;
    int height_sum = 0;
    int width_sum = 0;
    for(float current_scale = 1.0; current_scale > smallest_scale; current_scale*=SCALE_RATIO) 
    {
        ++num_stages;
    }

    int current_max_height = img_start_height/_shrink + SHRUNK_WINDOW_HEIGHT;
    float biggest_col_min_scale = 1.0;
    float current_scale = 1.0;
    bool success = false;

    // One layout attempt each iteration
    while(pyr_coords.size() == 0) {

        // cout << "\n=============new attempt==========\n";
        current_scale = 1.0;
        int col_width = 0;
        int col_offset = SHRUNK_WINDOW_WIDTH/2;
        int next_col_width = ceil((float)img_start_width*current_scale)/_shrink;
        int images_placed = 0;

        // One column done each iteration
        while(true) {
            if ((col_offset+next_col_width+SHRUNK_WINDOW_WIDTH/2) > pyramid_width) {
                pyr_coords.clear();
                break;
            }

            // cout << "-- new col:\n";

            int col_height = ceil((float)img_start_height*current_scale)/_shrink;
            int row_offset = SHRUNK_WINDOW_HEIGHT/2;
            int next_im_height = ceil((float)img_start_height*current_scale)/_shrink; 
            min_pyramid_width = 0;    

            // One element of a column each iteration
            while(  (row_offset+next_im_height + SHRUNK_WINDOW_HEIGHT/2) <= 
                    (current_max_height + SHRUNK_WINDOW_HEIGHT/2)
                 ) 
            {
                // cout << "current col height: " << (row_offset+next_im_height+WINDOW_HEIGHT/2) << " \tmax height: " << (current_max_height + WINDOW_HEIGHT/2) << "\n";
                //cout << "CW:" << col_width << "\tcurrent: " << img_start_width << "x" << current_scale << " = " << ceil((float)img_start_width*current_scale) << "\ncol_offset: " << col_offset << "\n";
                col_width = std::max<int>(col_width, ceil((float)img_start_width*current_scale)/_shrink);
                pyr_coords.push_back(cv::Point2i(col_offset, row_offset));
                min_pyramid_width = std::max<int>(min_pyramid_width, col_offset+col_width+SHRUNK_WINDOW_WIDTH/2);
                if(pyr_coords.size() == num_stages) {
                    success = true;
                    break;
                }

                row_offset += next_im_height;
                current_scale*=SCALE_RATIO;
                next_im_height = ceil((float)img_start_height*current_scale)/_shrink;
            }

            if(success == true) break; 

            col_offset += col_width + PYRAMID_COL_STEP;
            col_width = 0;
            next_col_width = ceil((float)img_start_width*current_scale)/_shrink;
        }

        biggest_col_min_scale *= SCALE_RATIO;
        pyramid_height = current_max_height + SHRUNK_WINDOW_HEIGHT/2;
        current_max_height += ceil((float)img_start_height*biggest_col_min_scale)/_shrink;
    }

    if(!success) {
        cerr << "Error: Couldn't find pyramid layout.\n";
        return;
    }

    return;
}
