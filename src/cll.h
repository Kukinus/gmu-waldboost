#ifndef CLL_H_
#define CLL_H_



#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <stdexcept>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>

#if defined(__APPLE__) || defined(__MACOSX)
#include <OpenCL/cl.hpp>
#else
#include <CL/cl.hpp>
#endif

#include "ocl_util.h"

class CL {
public:
    enum BufferType{OCL_POSITION_BUFFER, OCL_MVP, OCL_MV, OCL_MN, OCL_NORMAL_BUFFER, OCL_ELEMENT_ARRAY_BUFFER};

private:
    const cl::ImageFormat _uint8_format;
    const cl::ImageFormat _sint16_format;
    const cl::ImageFormat _float_format;

    //handles for creating an opencl context 
    //cl_platform_id platform;
    std::vector<cl::Platform> _platforms;
    const unsigned int _platform_used;
    
    //device variables
    //cl_device_id* devices;
    cl_uint _numDevices;
    const unsigned int _device_used;
    std::vector<cl::Device> _all_devices;
    cl::Device _device;

    // Info about the GPU
    cl_uint _max_work_group_size;
    std::vector<size_t> _max_work_item_sizes;
    cl_ulong _globalMemSize;
    cl_ulong _localMemSize;

    int _max_positions_per_thread;


    
    //cl_context context;
    cl::Context _context;

    //cl_command_queue command_queue;
    cl::CommandQueue _queue;
    std::string _kernel_source_path;
    std::ifstream _kernel_source_file;
    cl::Program _program;
    cl::Kernel _kernelGS;
    cl::Kernel _kernelShrink;
    cl::Kernel _kernelShrinkUnsigned;
    cl::Kernel _kernelShrinkSigned;
    cl::Kernel _kernelDiffx;
    cl::Kernel _kernelDiffy;
    cl::Kernel _kernelResize;
    cl::Kernel _kernelDetect;
    
    // The buffers
    cl::Buffer _cl_gaussian_pyramid;
    cl::Buffer _cl_gaussian_diffx_pyramid;
    cl::Buffer _cl_gaussian_diffy_pyramid;
    cl::Buffer _cl_fids;
    cl::Buffer _cl_thrs;
    cl::Buffer _cl_qhs;
    cl::Buffer _cl_out_hs;
    cl::Buffer _cl_detections;
    cl::Buffer _cl_num_detections;

    std::vector<cl::Image2D> _cl_images_scaled;
    cl::Image2D _cl_image_blurred;
    cl::Image2D _cl_image_diffx;
    cl::Image2D _cl_image_diffy;

    bool _image_loaded;

    //debugging variables
    std::string _phase;
    cl_int _err;
    cl::Event _event;

    bool _verbose;

    float _shrink;

    void checkError();
    //load an OpenCL program from a file
    void loadProgram(const int window_width, const int window_height, const int feature_limit, const int fids_limit, const int tree_depth, const int qhs_limit, const float k, const int q, const int shrink);

    // Moves a matrix (array) of size size located at ptr into buffer
    void bufferMatrix(float *ptr, size_t size, cl::Buffer &buffer);

public:
    //default constructor initializes OpenCL context and automatically chooses platform and device
    CL(const std::string &kernel_path, const int window_width, const int window_height, const int feature_limit, const int fids_limit, const int tree_depth, const int qhs_limit, const float k, const int q, const int shrink);
    //default destructor releases OpenCL objects and frees device memory
    ~CL();

    // Copies an array of vectors located at ptr with num_elements vectors into
    // a buffer specified by buffer_type 
    void bufferData(int width, int height, int pyramidWidth, int pyramidHeight, std::vector<int> &fids, std::vector<int> &thrs, std::vector<int> &qhs, float shrink);

    //execute the kernel
    int runKernels(cv::Mat &gray, float scaleRatio, float scaleInitial, int pyramidWidth, int pyramidHeight, std::vector<cv::Point2i>& pyramidCoords, cv::Mat &gausPyramid, std::vector<int> &detections, std::vector<int> &out_hs);

    void createPyramids(int width, int height, std::vector<cl::Image2D> &scaledImages, cl::Buffer &gaussPyramid, cl::Buffer &diffxPyramid, cl::Buffer &diffyPyramid, float scaleRatio, float scaleInitial, std::vector<cv::Point2i> &pyramidCoords, int pyramidWidth);


    unsigned long getGlobalMemSize();
    static unsigned int iCeilTo(unsigned int data, unsigned int align_size);
};

#endif // CLL_H_
