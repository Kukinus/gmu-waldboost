#include "face_detector.h"

using namespace std; 
using namespace cv;

const float FaceDetector::K = 0.5;     // because you can't initialize static floats in class definition
const float FaceDetector::SCALE_INITIAL = 1.5;
const float FaceDetector::SCALE_RATIO = 7.0/8.0;
const float FaceDetector::SCALE_RATIO_INV = 1.0/FaceDetector::SCALE_RATIO;

// Constructor.
FaceDetector::FaceDetector(const string & path) :
    _path(path),
    _initialized(false)
{
    _fids.reserve(FEATURE_COUNT*FIDS_LIMIT);
    _thrs.reserve(FEATURE_COUNT*FIDS_LIMIT);
    _qhs.reserve(FEATURE_COUNT*QHS_LIMIT);
}

FaceDetector::~FaceDetector()
{
    
}

// Face detector initialization.
bool FaceDetector::init()
{
    vector<int> tmpVec;
    _initialized = loadData(_path + "fids", _fids);
    _initialized &= loadData(_path + "thrs", _thrs);
    _initialized &= loadData(_path + "qhs", _qhs);
    _initialized &= loadData(_path + "shrink", tmpVec);

    if(_initialized){
        _shrink = tmpVec[0];
    }

    return _initialized;
}

// Loads data from file given by file path.
bool FaceDetector::loadData(const string& filePath, vector<int> &data)
{
    int number;
    fstream fp;

    fp.open(filePath.c_str(),fstream::in);
    if(fp.is_open()){
        while (fp >> number)
        {
            data.push_back(number);
        }
    }
    else{
        printf("Cannot load file: %s\n",filePath.c_str());
        return false;
    }
    fp.close();
    return true;
}
