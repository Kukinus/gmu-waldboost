#ifndef FACE_DETECTOR_GPU_H
#define FACE_DETECTOR_GPU_H

#include "face_detector.h"
#include "cll.h"

using namespace std;
using namespace cv;

//! Face detecotr class.
class FaceDetectorGPU : public FaceDetector {


public:
    // --- CONSTRUCTORS / DESTRUCTOR ---

    //! Constructor.
    FaceDetectorGPU(const string &path);

    //! Destructor.
    ~FaceDetectorGPU();

    // --- PUBLIC CLASS VARIABLES ---

    // --- PUBLIC METHODS ---

    bool init();

    //! Detects face occurences in the image.
    void compute(const Mat &image, std::vector<Rect> &rects);

    

private:
    // --- PRIVATE METHODS ---

    std::vector<cv::Rect> GenerateResultRectanglesDom(std::vector<int> &centers, int width, int height);
    std::vector<cv::Rect> GenerateResultRectanglesPyr(std::vector<int> &centers);

    void showPyramidLayout(const Mat &img);

    /*!
     * \brief For image pyramid: Calculates positions of the top left corner of
     *        each image in the pyramid image. The pyramid image would be padded 
     *        with empty space of size WINDOW_WIDTH/2 or WINDOW_HEIGHT/2 from each
     *        side. Main result will be in pyr_coords . The algorithm presumes that
     *        later at each point of pyr_coords an image will be inserted. Each image
     *        should have the size of SCALE_RATIO of the previous one.
     *
     * \param [in] img_start_width Width of the input image
     * \param [in] img_start_height Height of the input image
     * \param [in] max_pyramid_width Maximum allowed width of the pyramid image
     * \param [out] min_pyramid_width Minimum computed width of the pyramid image 
                    so that all shrunk images would still fit there would be no extra 
                    empty space on the right.
     * \param [out] pyramid_height The computed height of the pyramid image.
     * \param [out] pyr_coords The coordinates of the top left corner of each shrunk image.
     */
    void calculatePyramidLayout(int img_start_width, int img_start_height, int &min_pyramid_width, int &pyramid_height, std::vector<cv::Point2i> &pyr_coords);

    int calculateMaxPyramidWidth(int img_width, int img_height);

    // --- PRIVATE CLASS VARIABLES ---
    static const int PYRAMID_COL_STEP = 10;
    int _max_pyramid_width;

    CL * _clApi;

    int _img_height;
    int _img_width;
    int _pyramid_width;
    int _pyramid_height;
    std::vector<cv::Point2i> _pyr_coords;

};

#endif // FACE_DETECTOR_GPU_H
