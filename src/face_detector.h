#ifndef _FACE_DETECTOR_H_
#define _FACE_DETECTOR_H_

// Standard libraries.
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>

// Project libraries.
#include "image.hpp"
#include "stopwatch.h"
#include "nms.h"

using namespace std; 
using namespace cv;

//! Face detecotr class.
class FaceDetector {


public:
    // --- CONSTRUCTORS / DESTRUCTOR ---

    //! Constructor.
    FaceDetector(const string &path);

    //! Destructor.
    virtual ~FaceDetector();

    // --- PUBLIC CLASS VARIABLES ---

    static const float SCALE_INITIAL;
    static const float SCALE_RATIO;
    static const float SCALE_RATIO_INV;

    // --- PUBLIC METHODS ---

    //! Face detector initialization.
    virtual bool init();

    //! Detects face occurences in the image.
    virtual void compute(const Mat &image, std::vector<Rect> &rects) = 0;

protected:
    // --- PROTECTED METHODS ---

    //! Loads data from file given by file path.
    bool loadData(const string& filePath, vector<int> &data);

    // --- PROTECTED CLASS VARIABLES ---

    static const int FEATURE_COUNT = 2048;
    static const int FEATURE_LIMIT = 1500;
    static const int WINDOW_WIDTH = 64;
    static const int WINDOW_HEIGHT = 64;
    static const int TREE_DEPTH = 2;
    static const int FIDS_LIMIT = 3;
    static const int QHS_LIMIT = 4;
    static const int Q = -1024;
    static const float K; // can't initialize static floats in class
    // need to init them in the .cpp files

    string _path;
    vector<int> _fids;
    vector<int> _thrs;
    vector<int> _qhs;
    int _shrink;
    bool _initialized;

};

#endif // _FACE_DETECTOR_H_
