
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <chrono>
#include <thread>
#include <boost/filesystem.hpp>
#include <boost/scoped_ptr.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>

#include "face_detector_cpu.h"

#ifndef NO_GPU
   #include "face_detector_gpu.h"
#endif

#include "tracker.h"

using namespace std; 
namespace b_fs = boost::filesystem;

enum Switch {TO_FILE, USE_CPU, TRACK, SWITCH_ENUM_MAX = TRACK};
// SWITCH_ENUM_MAX now tells us the maximum legal value of the Switch enum

std::string programName;

void printHelp() {
    cout << "Usage:\n" << programName << " [--tofile] [--CPU] [--track] input_file classifier_data_folder\n\n";
    cout << "\t--tofile\t\tinstead of displaying images, the application will\n"
            "\t\t\t\tsave them in a video file\n";
    cout << "\t--CPU\t\t\tuse CPU implementation of the classifier\n";
    cout << "\t--track\t\t\tuse tracker after detecting faces\n";
    cout << "\tinput_file\t\tpath to an image file or video\n";
    cout << "\tclassifier_data_folder\tpath to folder with tree data files\n";
    cout << "\t\t\t\t(fids, qhs, thrs)\n\n";

    cout << "Alternatively you can set input_file to a number to read from a connected camera.\nFor example \"0\" will make the application read from the first camera.\n";
}


inline bool processSwitch(const string &arg, bool *switches)
{
   if (arg == string("--CPU")) {
      switches[USE_CPU] = true;
      return true;
   } else if (arg == string("--tofile")) {
      switches[TO_FILE] = true;
      return true;
   } else if (arg == string("--track")) {
      switches[TRACK] = true;
      return true;
   } else {
      cerr << "Error: Wrong arguments given.\n";
      printHelp();
      return false;
   }
} 


int main( int argc, char* argv[]){

    // All supported image formats by the OpenCV 3
    vector<string> img_formats;
    img_formats.push_back(string(".bmp")); img_formats.push_back(string(".dib"));
    img_formats.push_back(string(".jpeg")); img_formats.push_back(string(".jpg"));
    img_formats.push_back(string(".jpe")); img_formats.push_back(string(".jp2"));
    img_formats.push_back(string(".png")); img_formats.push_back(string(".webp"));
    img_formats.push_back(string(".pbm")); img_formats.push_back(string(".pgm"));
    img_formats.push_back(string(".ppm")); img_formats.push_back(string(".sr"));
    img_formats.push_back(string(".ras")); img_formats.push_back(string(".tiff"));
    img_formats.push_back(string(".tif"));

    /*********************** v Process args v ************************/
    bool switches[SWITCH_ENUM_MAX+1] = { false, };
    int argOffset = 0;

    programName = argv[0];

    if(argc == 2 && string(argv[1]) == "-h") {
        printHelp();
        return 0;
    }

    if (argc < 3 || argc > SWITCH_ENUM_MAX+3) {
        cerr << "Error: Wrong arguments given.\n";
        printHelp();
        return -1;
    }

    // iterate over switches preceding the filename argument
    for(int i = 1; i <= SWITCH_ENUM_MAX; ++i) {
      if(argc >= i+3) {
         if(processSwitch(string(argv[i]), switches)) argOffset += 1;
         else return -1;
      } else {
         break;
      }
    }

    String in_file(argv[1+argOffset]);
    String path(argv[2+argOffset]+String("/"));
    int captureDevice = -1;
    string extension;
    bool isImage = false;

    if(in_file.size() == 1 && isdigit(in_file[0])) {
        captureDevice = in_file[0] - '0';
        extension = ".mp4";
    } else {
        if(!b_fs::is_regular_file(b_fs::path(in_file))) {
           cerr << "Error: File \"" << in_file << "\" doesn't exist.\n";
           printHelp();
           return -1;
        }

        if(!b_fs::is_directory(b_fs::path(path))) {
           cerr << "Error: Folder \"" << path << "\" doesn't exist.\n";
           printHelp();
           return -1;
        }

        extension = b_fs::path(in_file).extension().string();
        isImage = std::find(img_formats.begin(), img_formats.end(), extension) != img_formats.end();
    }

    

    /*********************** ^ Process args ^ ************************/

    /************************* v Do work v ***************************/

    std::vector<Rect> rects;

    // scoped_ptr destroys referenced object when it goes out of scope
    boost::scoped_ptr<FaceDetector> det; 

    #ifndef NO_GPU
      if (switches[USE_CPU]) det.reset(new FaceDetectorCPU(path));
      else det.reset(new FaceDetectorGPU(path));
    #else
      det.reset(new FaceDetectorCPU(path));
    #endif
    

    if(!det->init()){
        cerr << "Error: Detector initialization failed!";
        return -1;
    }

    if(isImage) {                          // Image case
        Mat draw_image = imread(in_file);
        Mat gray_image;

        if(draw_image.data == NULL) {
            cerr << "Error: Could not read file \"" << in_file << "\" as image.\n";
            printHelp();
            return -1;
        }

        cv::cvtColor(draw_image, gray_image, CV_BGR2GRAY);

        //imshow("tmp", gray_image);
        //waitKey(0);

        det->compute(gray_image, rects);

        float scale = 1.0/FaceDetector::SCALE_INITIAL;
        if (!switches[USE_CPU]) {
            scale = 1.0;
        }

        //resize(draw_image, draw_image, Size(), FaceDetector::SCALE_INITIAL, FaceDetector::SCALE_INITIAL, INTER_LINEAR);

        std::vector<cv::Rect>::iterator it = rects.begin();
        for(; it != rects.end(); ++it){
            cv::Rect r(it->x*scale, it->y*scale, it->width*scale, it->height*scale);  
            rectangle(draw_image, r, Scalar(0, 255, 0));
        }

        //if (switches[USE_CPU]) {
        if(true) {  
            imshow( "Face detector", draw_image);
            waitKey();
        }
        

    } else {                               // Video case

        // Init
        VideoCapture capture;
        //Tracker tracker;

        boost::scoped_ptr<Tracker> tracker;
        if(switches[TRACK]) tracker.reset(new Tracker);

        if(captureDevice >= 0) capture.open(captureDevice);
        else capture.open(in_file);

        if (!capture.isOpened()) {
            cerr << "Error: Could not read file \"" << in_file << "\" as video.\n" << endl;
            return 1;
        }

        const string outputVideoFileName = "./videoOut" + extension;
        VideoWriter writer;
        if (switches[TO_FILE]) {
            writer.open( outputVideoFileName, 
                         capture.get( CV_CAP_PROP_FOURCC), 
                         capture.get(CV_CAP_PROP_FPS), 
                         Size(capture.get(CV_CAP_PROP_FRAME_WIDTH), 2*capture.get(CV_CAP_PROP_FRAME_HEIGHT))
                       );
            if( !writer.isOpened()){
               cerr << "Error: Unable to open output video file \"" << outputVideoFileName << "\"." << endl;
                 return -1;
            }

            cout << "Processing input video..." << std::flush;
        }
        

         
        Mat draw_image, gray_image, hsv_image, track_image;
        int video_height = capture.get(CV_CAP_PROP_FRAME_HEIGHT);
        if(switches[TRACK]) video_height *= 2;
        Mat video_image(video_height, 
                        capture.get(CV_CAP_PROP_FRAME_WIDTH), 
                        CV_8UC3 );

        // Vars needed for displaying the video with the right framerate
        int fps_of_video = (int)capture.get(CAP_PROP_FPS);
        int time_to_wait = 1000 / fps_of_video;

        // Do work
        for (;;) {
            double time_start = (double)getTickCount();

            capture >> draw_image;
            if (draw_image.empty())
                break;

            rects.clear();

            cv::cvtColor(draw_image, gray_image, CV_BGR2GRAY);
         
            det->compute(gray_image, rects);

            float scale = 1.0/FaceDetector::SCALE_INITIAL;
            if (!switches[USE_CPU]) {
               scale = 1.0;
            }
            //resize(draw_image, draw_image, Size(), FaceDetector::SCALE_INITIAL, FaceDetector::SCALE_INITIAL, INTER_LINEAR);

            std::vector<cv::Rect>::iterator it = rects.begin();
            for(; it != rects.end(); ++it){
                *it = cv::Rect(it->x*scale, it->y*scale, it->width*scale, it->height*scale);  
                //rectangle(draw_image, *it, Scalar(0, 255, 0));
            }

            if(switches[TRACK]) {
               cv::cvtColor(draw_image, hsv_image, CV_BGR2HSV );
               track_image = draw_image.clone();
               tracker->nextFrame(gray_image, hsv_image);
               bool changed = tracker->includeRects(rects);
               tracker->visualizeTracks(track_image);
            }
            

            it = rects.begin();
            for(; it != rects.end(); ++it){ 
                rectangle(draw_image, *it, Scalar(0, 255, 0));
            }

            if(!switches[TO_FILE]) {
               imshow( "Face detector", draw_image);
               if(switches[TRACK]) imshow( "Tracker", track_image);
            } else {

               /*
               // Write image with detections and image with Tracks to video file.
               // Both images will be in one frame.  
               draw_image.copyTo(video_image(cv::Rect(0,0,draw_image.cols, draw_image.rows)));
               if(switches[TRACK]) {
                  track_image.copyTo(video_image(cv::Rect(0,capture.get(CV_CAP_PROP_FRAME_HEIGHT),track_image.cols, track_image.rows)));
               }
               if(captureDevice >= 0) imshow( "Video", video_image);
               writer.write( video_image);
               */

            }
  
            //if (changed) waitKey();
            int waiting_time = 1;
            if(switches[USE_CPU] && !switches[TO_FILE]) waiting_time = 50;

            char c;
            if ((c = (char)waitKey(waiting_time)) == 27 || c == 'q') break;

            if(!switches[TO_FILE]) {
               // wait for some time to correct FPS
               while (time_to_wait > ((double)getTickCount() - time_start) / getTickFrequency() * 1000)
               {
                   std::this_thread::sleep_for(std::chrono::milliseconds(2));
               }
            }
            
        }

        if(switches[TO_FILE]) cout << "  done.\n";
    }

    return 0;
}
