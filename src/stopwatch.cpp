#include "stopwatch.h"

StopWatch::StopWatch(const char *what, Units units) :
	_what(what),
	_isStarted(false),
	_units(units)
{
	start();
}

StopWatch::~StopWatch() {
	stop();
}

void StopWatch::start() {
	_startTime = cr::high_resolution_clock::now();
	_isStarted = true;
}

void StopWatch::stop() {
	if(_isStarted) {
		auto endTime = cr::high_resolution_clock::now();
		auto diff = endTime - _startTime;

		std::cout << _what << " took ";
		if(_units == S) {
		    auto duration = cr::duration_cast<cr::seconds>(diff);
		    std::cout << duration.count() << " s.\n";
	    } else if(_units == MS) {
		    auto duration = cr::duration_cast<cr::milliseconds>(diff);
		    std::cout << duration.count() << " ms.\n";
	    } else if(_units == US) {
		    auto duration = cr::duration_cast<cr::microseconds>(diff);
		    std::cout << duration.count() << " us.\n";
	    } else if(_units == NS) {
		    auto duration = cr::duration_cast<cr::nanoseconds>(diff);
		    std::cout << duration.count() << " ns.\n";
	    }

	    _isStarted = false;
	}
}
