#include "tracker.h"

// Cannot init static const objects in class declaration. They need to be initialized here.
// Constants for the goodFeaturesToTrack() function
const int               Tracker::MAX_FEATURE_COUNT = 500;
const float             Tracker::FEATURE_QUALITY = 0.2f;
const int               Tracker::MIN_FEATURE_DISTANCE = 7;
const int               Tracker::BLOCK_SIZE = 7;
const bool              Tracker::USE_HARRIS = false;
const float             Tracker::K = 0.04f;
const cv::Size          Tracker::_subPixWinSize(10,10);

// Constants for the calcOpticalFlowPyrLK() function
const int               Tracker::MAX_PYR_LEVEL = 3;
const cv::TermCriteria  Tracker::TERM_CRIT(TermCriteria::COUNT|TermCriteria::EPS,10,0.03);
const cv::Size          Tracker::_winSize(31,31);

const float             Tracker::MIN_EIG_THRESH = 0.001f;


const char              Tracker::STARTING_TIME_TO_LIVE = 15;

const bool              Tracker::_DEBUG = false;



Tracker::Tracker() :
    _points(2),
    _nightMode(false),
    _nextID(0)
{
    _points[0].resize(0);
    _points[1].resize(0);
}

Tracker::~Tracker() {
    
}

size_t Tracker::getNewID() { return _nextID++; }



bool Tracker::getOverlap( const Rect &r, std::vector< std::list<struct Track>::iterator > &track_its, size_t &retaken_id)
{
    bool found_overlap = false;
    track_its.clear();

    // Check if the new rectangle overlaps with some of the already tracked ones
    std::list<struct Track>::iterator track_it = _tracks.begin();
    for(; track_it != _tracks.end(); ++track_it ) {

        float smaller_area = (float)std::min<int>(track_it->rect.area(), r.area());
        // if the two rectangles overlap by 0.3 times the size of the smaller one
        if( (float)(track_it->rect & r).area() / smaller_area > 0.3) { 
            retaken_id = track_it->id;
            found_overlap = true;
            track_its.push_back(track_it);
        }
    }

    return found_overlap;
}



bool Tracker::includeRects(const vector<Rect> &rects) {

    bool tracksChanged = false;

    if(_prevGray.empty()) {
        cerr << "Warning: Cannot start tracking areas because no image has yet been given to the tracker. Use nextFrame() first.\n";
        return tracksChanged;
    }

    if(rects.size() == 0) return tracksChanged;

    if(_DEBUG) {
        cout << "\n******************* Tracks before:  ************************\n";
        printTracks();
    }
    

    size_t retaken_id;
    bool isOverlapping = false;
    Mat mask = cv::Mat::zeros(_prevGray.rows, _prevGray.cols, CV_8UC1);
    vector<Rect>::const_iterator it = rects.begin();
    for(; it != rects.end(); ++it) {
        int rect_x = std::max<int>(it->x, 0);
        int rect_y = std::max<int>(it->y, 0);
        int rect_width = it->width - std::min<int>(it->x, 0);
        int rect_height = it->height - std::min<int>(it->y, 0);
        Rect constrained_rect(rect_x, rect_y, rect_width, rect_height);
        Rect padded_rect(rect_x+BLOCK_SIZE, rect_y+BLOCK_SIZE, rect_width-2*BLOCK_SIZE, rect_height-2*BLOCK_SIZE);

        cv::rectangle(mask, padded_rect, Scalar(255), CV_FILLED);

        std::vector< std::list<struct Track>::iterator > track_its;
        isOverlapping = getOverlap(constrained_rect, track_its, retaken_id);
        if(isOverlapping) {
            if(_DEBUG) cout << "-- erasing track(s) due to overlap:\n";
            for(int i = 0; i < track_its.size(); ++i) {
                if(_DEBUG) printTrack(*(track_its[i]));
                deleteTrack(track_its[i]);
            }
        }

        std::vector<cv::Point2f> pts;

        goodFeaturesToTrack(_prevGray, 
                            pts, 
                            MAX_FEATURE_COUNT, 
                            FEATURE_QUALITY, 
                            MIN_FEATURE_DISTANCE, 
                            mask,                  // mask
                            BLOCK_SIZE, 
                            USE_HARRIS, 
                            K  );

        if(pts.size() != 0)  {
            cornerSubPix(_prevGray, pts, _subPixWinSize, Size(-1,-1), TERM_CRIT);

            Point2f sum(0.0,0.0);
            for(auto pit = pts.begin(); pit != pts.end(); ++pit) {
                sum += *pit;
            }
            Point2f avg(sum / (float)pts.size());
            Point2f offset(Point2f(rect_x, rect_y) - avg);

            size_t start_index = _points[1].size();
            size_t end_index = _points[1].size()+pts.size()-1;

            struct Track t = { isOverlapping ? retaken_id : getNewID(),  // id
                               *it,         // rect
                               offset,      // offset of rectangle origin from average point
                               end_index - start_index + 1,  // original point count
                               start_index,  // start index in _points[1]
                               end_index,   // end index
                               STARTING_TIME_TO_LIVE  }; 

            if(_DEBUG) cout << "size before insert: " << _points[1].size() << "\n";
            _points[1].insert( _points[1].end(), pts.begin(), pts.end() );
            if(_DEBUG) cout << "size after insert: " << _points[1].size() << "\n";
            _tracks.push_back(t);

            tracksChanged = true;
        }

        isOverlapping = false;

        cv::rectangle(mask, constrained_rect, Scalar(0), CV_FILLED);

    }
    if(!checkTrackConsistency()) {
        if(_DEBUG) {
            cout << "\t\t\t\tInconsistency found at place 1!\n";
            printTracks();
        }
        
        clearTracks();
        return false;
    }

    if(_DEBUG) {
        cout << "******************* Tracks after:  ************************\n";
        printTracks();
    }
    

    return tracksChanged;
}



void Tracker::processPoints(std::vector<cv::Point2f> &points, std::vector<uchar> &keepThisPoint)
{
    if(_DEBUG) cout << "\nsize before reduce: " << points.size() << "\n";

    // Remove points for which we lost track (keepThisPoint[i] is false)
    size_t i, k;
    size_t points_deleted = 0;
    Point2f sum(0.0,0.0);
    std::list<struct Track>::iterator track_it = _tracks.begin();
    for( i = k = 0; i < points.size(); i++ )
    {
        if(_DEBUG) cout << "i: " << i << "\tk: " << k << endl;

        // Does points[i] belong to the next track? We should process the current one then...
        if(track_it != _tracks.end() && i > track_it->end) {
            track_it->end -= points_deleted;
            track_it->time_to_live -= 1;

            // Is there too few points left to track? Or did the track's time_to_live expire?
            // Then we should delete it!
            long current_point_cnt = (long)track_it->end - (long)track_it->start + 1;
            long point_cnt_thresh = (track_it->original_point_cnt)/3;
            if (current_point_cnt < point_cnt_thresh || 
                current_point_cnt < 3 || 
                track_it->time_to_live == 0) 
            {
                if(_DEBUG) {
                    cout << "-- erasing track due to lost points or expired time_to_live:\n";
                    printTrack(*track_it);
                }

                if(current_point_cnt > 0) {
                    keepThisPoint.erase(keepThisPoint.begin()+track_it->start, keepThisPoint.begin()+track_it->end+1);
                    i = i - current_point_cnt;
                    k = k - current_point_cnt;
                }
                deleteTrack(track_it); // track_it then points to next element 
            }
            else {
                Point2f avg(sum/ ((float)track_it->end - (float)track_it->start + 1.0));
                Point2f newOrigin(avg + track_it->offset_from_avg);

                if(_DEBUG) cout << "!!New avg for Track " << track_it->id << ": " << avg << "\n";

                track_it->rect.x = newOrigin.x;
                track_it->rect.y = newOrigin.y;

                ++track_it;

                // size_t next_start = track_it->end + 1;
                // ++track_it;
                // long diff = (long)track_it->end - (long)track_it->start;
                // track_it->start = next_start;
                // track_it->end = next_start + diff;
            }

            if(track_it != _tracks.end()) track_it->start -= points_deleted;
            --i;
            if(_DEBUG) cout << "suma = 0" << endl;
            sum.x = sum.y = 0.0;
            continue;
        }



        if( !keepThisPoint[i] ) {
            ++points_deleted;
            continue;
        }

        points[k] = points[i];
        if(_DEBUG) cout << "suma += " << points[k] << endl;
        sum += points[k];
        ++k;
    } // for

    if(track_it != _tracks.end()) {
        track_it->end -= points_deleted;
        Point2f avg(sum/ ((float)track_it->end - (float)track_it->start + 1.0));
        Point2f newOrigin(avg + track_it->offset_from_avg);

        if(_DEBUG) cout << "!!New avg for Track " << track_it->id << ": " << avg << "\n";

        track_it->rect.x = newOrigin.x;
        track_it->rect.y = newOrigin.y;
    }
    points.resize(k);

    if(_DEBUG) cout << "size after reduce: " << points.size() << "\n";
}



bool Tracker::sceneChanged( const Mat &img_hsv )
{
    static const int h_bins = 50; 
    static const int s_bins = 60;
    static const int histSize[] = { h_bins, s_bins };

    static const float h_ranges[] = { 0, 180 };
    static const float s_ranges[] = { 0, 256 };

    static const float* ranges[] = { h_ranges, s_ranges };

    static const int channels[] = { 0, 1 };

    static const double threshold = 4.0;

    MatND new_hist;
    bool sceneChanged = false;

    calcHist( &img_hsv, 1, channels, Mat(), new_hist, 2, histSize, ranges, true, false );
    normalize( new_hist, new_hist, 0, 1, NORM_MINMAX, -1, Mat() );

    if(!_prevHSV.empty()) {
        // double score = compareHist( new_hist, _prevHSVHistogram, HISTCMP_CORREL );
        double score = compareHist( new_hist, _prevHSVHistogram, HISTCMP_CHISQR );
        // double score = compareHist( new_hist, _prevHSVHistogram, HISTCMP_INTERSECT );
        // double score = compareHist( new_hist, _prevHSVHistogram, HISTCMP_BHATTACHARYYA  );
        //cout << "++ Diff score: " << score << "\n";
        if (score > threshold) sceneChanged = true;
    }
    
    _prevHSV = img_hsv.clone();
    _prevHSVHistogram = new_hist;

    return sceneChanged;
}


void Tracker::nextFrame( const Mat &frame_gray, const Mat &frame_hsv )
{
    Mat gray, image;

    frame_gray.copyTo(gray);

    if( _nightMode )
        image = Scalar::all(0);

    if(sceneChanged(frame_hsv)) {
        clearTracks();
    }
    
    _points[0] = _points[1];

    if( !_points[0].empty() )
    {
        vector<uchar> status;
        vector<float> err;
        if(_prevGray.empty())
            gray.copyTo(_prevGray);

        calcOpticalFlowPyrLK(_prevGray, 
                             gray, 
                             _points[0], 
                             _points[1], 
                             status,        // Which points could we keep track of?
                             err, 
                             _winSize,
                             MAX_PYR_LEVEL, // max pyramid level
                             TERM_CRIT, 
                             0,             // flags
                             MIN_EIG_THRESH  ); // minimum eigenvalue threshold
        
        processPoints(_points[1], status);
    }

    char c = (char)waitKey(10);
    // if( c == 27 )
    //     break;
    switch( c )
    {
    case 'c':
        _points[0].clear();
        _points[1].clear();
        break;
    case 'n':
        _nightMode = !_nightMode;
        break;
    }

    //std::swap(_points[1], _points[0]);
    //cv::swap(_prevGray, gray);
    _prevGray = gray;

    return;
}


void Tracker::deleteTrack( std::list<struct Track>::iterator &track_it )
{
    int deleted_size = track_it->end - track_it->start + 1;

    if(_DEBUG) cout << "size before erase1: " << _points[1].size() << "\n";
    if (deleted_size > 0) {
        _points[1].erase(_points[1].begin()+track_it->start, _points[1].begin()+track_it->end+1);
    }
    if(_DEBUG) cout << "size after erase1: " << _points[1].size() << "\n";

    track_it = _tracks.erase(track_it); // track_it then points to next element

    std::list<struct Track>::iterator it2 = track_it;
    for(; it2 != _tracks.end(); ++it2 ) {
        it2->start -= deleted_size;
        it2->end -= deleted_size;
    }
}

void Tracker::clearTracks() {
    _points[0].clear();
    _points[1].clear();
    _tracks.clear();
}


void Tracker::visualizeTracks( Mat &img )
{
    std::vector<cv::Point2f>::iterator pit = _points[1].begin();
    std::list<struct Track>::iterator tit = _tracks.begin();
    for(int i = 0; pit != _points[1].end(); ++pit, ++i) {

        if(i == tit->start) {
            std::stringstream ss;
            ss << "ID: " << tit->id;
            putText(img, ss.str(), Point(tit->rect.x, tit->rect.y-5), cv::FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0,255,0), 2, cv::LINE_8, false );
            rectangle(img, tit->rect, Scalar(0, 255, 0));
        }
        circle( img, *pit, 3, Scalar(0,255,0), -1, 8);
        if(i == tit->end) ++tit;
    }
}


bool Tracker::checkTrackConsistency() {

    size_t expected_start = 0;
    std::vector<cv::Point2f>::iterator pit = _points[1].begin();
    std::list<struct Track>::iterator tit = _tracks.begin();
    for(int i = 0; pit != _points[1].end(); ++pit, ++i) {

        if(tit == _tracks.end() || tit->start != expected_start) {
            cerr << "Error: Track inconsistency detected. Resetting Tracks.\n";
            if(_DEBUG) cout << "last_index: " << expected_start-1 << "\tpoints vec size: " << _points[1].size() << "\n";
            return false;
        }

        if(i == tit->end) {
            ++tit;
            expected_start = i+1;
        }
    }

    return true;
}


void Tracker::printTracks() {
    
    size_t expected_start = 0;
    std::vector<cv::Point2f>::iterator pit = _points[1].begin();
    std::list<struct Track>::iterator tit = _tracks.begin();
    for(int i = 0; pit != _points[1].end(); ++pit, ++i) {

        if(i == tit->start) {
            std::stringstream ss;
            ss << "\n======  Track ID " << tit->id << "  ======\n";
            ss << "Rect: x " << tit->rect.x << "\ty " << tit->rect.y << "\tw " << tit->rect.width << "\th " << tit->rect.height << "\n";
            ss << "offset " << tit->offset_from_avg << "\tstart " << tit->start << " \tend " << tit->end << "\ttime to live " << (int)tit->time_to_live <<  "\n";
            cout << ss.str();
        }

        if(i == tit->end) {
            ++tit;
        }

        cout << "Point: " << *pit << "\n";
    }
}



void Tracker::printTrack(const struct Track &t) {

    cout << "\nTrack ID " << t.id << "\n";
    cout << "Rect: x " << t.rect.x << "\ty " << t.rect.y << "\tw " << t.rect.width << "\th " << t.rect.height << "\n";
    cout << "offset " << t.offset_from_avg << "\tstart " << t.start << "\tend " << t.end << "\ttime to live " << (int)t.time_to_live << "\n\n";

}