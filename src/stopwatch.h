#ifndef STOPWATCH_H
#define STOPWATCH_H

// Standard libraries.
#include <iostream>
#include <chrono>

using namespace std;
namespace cr = std::chrono;

/** \brief Class that starts to measure time passed since instantiation
           until destruction. Automatic start can be disabled with
           the constructor's autoStart parameter.
 */
class StopWatch {

public:
    enum Units {S, MS, US, NS};

    // --- CONSTRUCTORS / DESTRUCTOR ---

    //! Constructor.
    StopWatch(const char *what, Units units=MS);

    //! Destructor.
    ~StopWatch();

    // --- PUBLIC CLASS VARIABLES ---

    // --- PUBLIC METHODS ---
    void start();
    void stop();

private:
    // --- PRIVATE METHODS ---


    
    // --- PRIVATE CLASS VARIABLES ---

    string _what;
    cr::high_resolution_clock::time_point _startTime;
    bool _isStarted;
    Units _units;

};

#endif // STOPWATCH_H
