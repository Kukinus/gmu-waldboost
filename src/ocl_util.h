//from http://www.songho.ca/opengl/gl_vbo.html

#ifndef OCL_UTIL_H_
#define OCL_UTIL_H_

#if defined(__APPLE__) || defined(__MACOSX)
#include <OpenCL/cl.hpp>
#else
#include <CL/cl.hpp>
#endif

const char* oclErrorString(cl_int error);

#endif // OCL_UTIL_H_
