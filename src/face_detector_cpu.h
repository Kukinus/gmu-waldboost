#ifndef _FACE_DETECTOR_CPU_H_
#define _FACE_DETECTOR_CPU_H_

/* Tento zdrojový kód a přilohy jsou předmětem autorských práv FIT VUT v Brně. Pro případné komerční využití a šíření je vyžadován písemný souhlas CEMC. Bez vědomí této organizace je zakázáno aplikaci kopírovat a dále distribuovat.
/* Tyto zdrojové kody a přílohy jsou poskytnuty pro spolupráci firmě Camea spol s r.o. a Eyedea Recognition s r.o. */

#include "face_detector.h"

using namespace std; 
using namespace cv;

//! Face detecotr class.
class FaceDetectorCPU : public FaceDetector {


public:
    // --- CONSTRUCTORS / DESTRUCTOR ---

    //! Constructor.
    FaceDetectorCPU(const string &path);

    //! Destructor.
    ~FaceDetectorCPU() {;}

    // --- PUBLIC CLASS VARIABLES ---

    // --- PUBLIC METHODS ---

    //! Detects face occurences in the image.
    void compute(const Mat &image, std::vector<Rect> &rects);

private:
    // --- PRIVATE METHODS ---

    //!
    void getCoords(int pos,int shrink, int &chan, int &x, int &y);

    // --- PRIVATE CLASS VARIABLES ---

};

#endif // _FACE_DETECTOR_CPU_H_
