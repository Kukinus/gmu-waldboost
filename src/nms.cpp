#include <vector>
#include <tuple>
#include <opencv2/opencv.hpp>
#include "nms.h"

using namespace std;
using namespace cv;

// Overlap of two rectangles
float rectOverlap(const Rect & a, const Rect & b, bool useMin = false)
{
    int x0 = max(a.x, b.x);
    int x1 = min(a.x+a.width, b.x+b.width);
    int y0 = max(a.y, b.y);
    int y1 = min(a.y+a.height, b.y+b.height);
    if (x0 >= x1 || y0 >= y1) return 0.f;
    float area = (x1 - x0) * (y1 - y0);
    float u;
    if (useMin)
        u = min((a.width * a.height),(b.width * b.height));
    else
        u = (a.width * a.height) + (b.width * b.height) - area;
    return area / u;
}

struct group_data
{
    cv::Rect new_pos;
    cv::Rect pos;
    float hs;
    int count;
};

float get_old_perc(float old_perc, float new_perc)
{
    return old_perc / (old_perc + new_perc);
}

float get_new_perc(float old_perc, float new_perc)
{
    return new_perc / (old_perc + new_perc);
}

void nmsMaxLinearImproved(vector<Rect> & rects, vector<int> & hs, float ovr, int minGroup)
{
    int N = rects.size();

    // Sort hs and get order
    vector<int> index(N);
    for(int i = 0; i < N; ++i) index[i] = i;
    // Sorting index vector but comparing values in hs
    sort(begin(index), end(index), [&hs](int u, int v)->bool {return hs[u]>hs[v]; });
    vector<group_data> g_data;
    // Suppress (add to group i) each j>i which was not already suppressed, and overlap(i,j)>ovr
    for(int _i = 0; _i < N; ++_i)
    {
        int i = index[_i];
        bool group_find = false;
        for(int j = 0; j < g_data.size(); ++j)
        {
            if(rectOverlap(rects[i], g_data[j].pos) > ovr)
            {
                float old_perc = get_old_perc(g_data[j].hs, hs[i]);
                float new_perc = get_new_perc(g_data[j].hs, hs[i]);
                g_data[j].count++;
                g_data[j].new_pos.x = g_data[j].new_pos.x * old_perc + rects[i].x * new_perc;
                g_data[j].new_pos.y = g_data[j].new_pos.y * old_perc + rects[i].y * new_perc;
                g_data[j].new_pos.width = g_data[j].new_pos.width * old_perc + rects[i].width * new_perc;
                g_data[j].new_pos.height = g_data[j].new_pos.height * old_perc + rects[i].height * new_perc;
                group_find = true;
                break;
            }
        }
        if(!group_find)
        {
            g_data.push_back({ rects[i], rects[i], (float)hs[i], 1 });
        }
    }

    vector<Rect> nmsRects;
    vector<int> nmsHs;

    for(int i = 0; i < g_data.size(); ++i)
        if(g_data[i].count >= minGroup)
        {
            nmsRects.push_back(g_data[i].new_pos);
            nmsHs.push_back((int)(g_data[i].hs));
        }

    rects = nmsRects;
    hs = nmsHs;
}

