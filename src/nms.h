#ifndef _NMS_H_
#define _NMS_H_

#include <vector>
#include <opencv2/opencv.hpp>

void nmsMaxLinearImproved(std::vector<cv::Rect> & rects, std::vector<int> & hs, float ovr = 0.5, int minGroup = 1);

#endif // _NMS_H_
