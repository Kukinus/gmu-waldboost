#include "face_detector_cpu.h"

// Constructor.
FaceDetectorCPU::FaceDetectorCPU(const string & path) :
    FaceDetector(path)
{

}

// Detects face occurences in the image.
void FaceDetectorCPU::compute(const Mat &image, std::vector<Rect> &rects) {

    Mat gray;
    image.convertTo(gray,CV_16S);

    std::vector<int> out_hs;
    std::vector<Rect> rects_temp;
    std::vector<int> out_hs_temp;

    resize(gray, gray, Size(), SCALE_INITIAL, SCALE_INITIAL, INTER_LINEAR);

    cout<<"Resolution: "<<gray.rows<<" x "<<gray.cols <<" dims "<< gray.dims<<endl;

    printf("Shrink: %d\n",_shrink);
    int stat_pos = 0;
    int stat_feature = 0;
    int stat_detect = 0;
    int max_suma = 0;

    for(int scale = 0; scale < 8; scale++){
        MImage<float> img(gray);
        MImage<float> chan[] = {img.triangle().shrink(_shrink), img.triangle().diffx().shrink(_shrink), img.triangle().diffy().shrink(_shrink)};

        for(int y = 0; y < chan[0].height - WINDOW_HEIGHT/_shrink; y+=2){
            for(int x = 0; x < chan[0].width - WINDOW_WIDTH/_shrink; x+=2){
                stat_pos++;
                int suma = 0;
                for(int i = 0; i < FEATURE_LIMIT; i++){
                    int node = 0;
                    stat_feature++;
                    for(int l = 0; l < TREE_DEPTH; l++){
                        int pos = _fids[i*FIDS_LIMIT+ node];
                        int th = _thrs[i*FIDS_LIMIT+ node];
                        int chan_c, x_c, y_c;
                        getCoords(pos,_shrink,chan_c,x_c,y_c);
                        float val = chan[chan_c](y+y_c,x+x_c);

                        if(val < th){
                            node = node*2+1;
                        }
                        else{
                            node = node*2+2;
                        }
                    }

                    int qhs_i = i*QHS_LIMIT+ (node-3);
                    int hs = _qhs[qhs_i];
                    suma+= hs;

                    if(suma < (K*i+Q))
                        break;
                    if(i == FEATURE_LIMIT-1){
                        cout<<"y: "<<y<<" x: "<<x<<" s: "<<scale<<" suma: "<<suma<<endl;
                        int s = scale;
                        Rect pt;
                        pt.x = _shrink*x;
                        pt.y = _shrink*y;
                        pt.width = WINDOW_WIDTH;
                        pt.height = WINDOW_HEIGHT;
                        while(s > 0){
                            pt.x= pt.x*SCALE_RATIO_INV;
                            pt.y= pt.y*SCALE_RATIO_INV;
                            pt.width = pt.width *SCALE_RATIO_INV;
                            pt.height = pt.height *SCALE_RATIO_INV;
                            s--;
                        }

                        rects.push_back(pt);
                        out_hs.push_back(suma);
                        stat_detect++;
                        if(suma > max_suma) max_suma = suma;
                    }
                }
            }
        }

        resize(gray, gray, Size(), SCALE_RATIO, SCALE_RATIO, INTER_LINEAR);
    }

    // draw all detection
    //for(int i = 0; i < rects.size(); i++) rectangle(draw_image, rects[i], Scalar(255.0f * out_hs[i] / max_suma, 255.0f * out_hs[i] / max_suma, 255.0f * out_hs[i] / max_suma));
    rects_temp = rects;
    out_hs_temp = out_hs;
    // std::cout << "Before NMS: " << rects_temp.size() << " detections.\n";
    nmsMaxLinearImproved(rects_temp, out_hs_temp);
    // std::cout << "After NMS: " << rects_temp.size() << " detections.\n";
    rects = rects_temp;
    printf("%d  %d  %f  %d\n",stat_pos,stat_feature, stat_feature/(stat_pos*1.0), stat_detect);

    // for(int j= 0; j<rects.size(); ++j) {
    //     cout<<"y: "<<rects[j].y<<" x: "<<rects[j].x<<" suma: "<<out_hs_temp[j]<<endl;
    // }
}

//
void FaceDetectorCPU::getCoords(int pos, int shrink, int &chan, int &x, int &y){

    chan = pos /((WINDOW_WIDTH/shrink)*(WINDOW_HEIGHT/shrink));
    x = (pos / (WINDOW_WIDTH/shrink)) % (WINDOW_HEIGHT/shrink);
    y = pos % (WINDOW_WIDTH/shrink);
}

